﻿using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Threading;

namespace smartenit
{
    public partial class MainForm : Form
    {
        Net net = null;
        bool started = false;

        public MainForm()
        {
            InitializeComponent();
            this.net = new Net();
            this.net.setForm(this);
        }

        public void log(string message)
        {
            this.tbLog.Text += message + "\r\n";
        }

        private void bConnect_Click(object sender, EventArgs e)
        {
            if (this.tbServer.Text == "" || this.tbPort.Text == "" || this.tbPasword.Text == "")
            {
                this.log("specify server, port and password");
                return;
            }
            string hash = (new SHA1Managed()).ComputeHash(Encoding.UTF8.GetBytes(this.tbPasword.Text)).ToString();
            this.net.connect(this.tbServer.Text, Int32.Parse(this.tbPort.Text), hash);
            this.cbDevices.Items.Clear();
            this.cbDevices.Items.AddRange(this.net.getDevices());
            if (this.cbDevices.Items.Count > 0)
            {
                this.cbDevices.Enabled = true;
                this.bStart.Enabled = true;
            }
        }

        private void bStart_Click(object sender, EventArgs e)
        {
            if (this.bStart.Text == "Start")
            {
                if (this.cbDevices.SelectedIndex == -1)
                {
                    this.log("choose device");
                    return;
                }
                this.bStart.Text = "Stop";
                this.chart.Series[0].Points.Clear();
                this.chart.Legends[0].Name = this.cbDevices.Items[this.cbDevices.SelectedIndex].ToString();
                this.cbDevices.Enabled = false;
                this.started = true;
                this.log("start collecting info for device");
                getDeviceInfoDelegate gdiD = new getDeviceInfoDelegate(this.getDeviceInfo);
                gdiD.BeginInvoke(this.chart.Legends[0].Name, null, null);
            }
            else
            {
                this.started = false;
            }
        }

        private delegate void getDeviceInfoDelegate(string device);

        private void getDeviceInfo(string device)
        {
            while (true)
            {
                string[] data = this.net.getDeviceInfo(device);
                Random r = new Random();
                bool stop = this.updateChart(Int32.Parse(data[r.Next(0, data.Length)]));
                if (stop)
                {
                    break;
                }
                Thread.Sleep(2000);
            }
        }

        private delegate bool updateChartDelegate(int val);

        private bool updateChart(int val)
        {
            if (this.chart.InvokeRequired == false)
            {
                if (this.chart.Series[0].Points.Count > 20)
                {
                    this.chart.Series[0].Points.RemoveAt(0);
                }
                this.chart.Series[0].Points.AddY(val);
                if (!this.started)
                {
                    this.bStart.Text = "Start";
                    this.cbDevices.Enabled = true;
                    this.log("stop collecting info for device");
                }
                return !this.started;
            }
            else
            {
                updateChartDelegate ucD = new updateChartDelegate(this.updateChart);
                object ret = this.Invoke(ucD, new object[] { val });
                return (bool)ret;
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.started = false;
            this.net.close();
        }
    }
}