﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using System.Threading;

namespace smartenit
{
    public class Net
    {
        private Socket socket = null;
        private Xml xml = null;
        private Boolean debug = false;
        private MainForm form = null;
        private const int bufferSize = 1024;
        private byte[] buffer = new byte[bufferSize];
        private StringBuilder sb = new StringBuilder();
        private static ManualResetEvent connectDone = new ManualResetEvent(false);
        private static ManualResetEvent sendDone = new ManualResetEvent(false);
        private static ManualResetEvent receiveDone = new ManualResetEvent(false);

        private void send(string msg)
        {
            Net.sendDone.Reset();
            byte[] b = Encoding.UTF8.GetBytes(msg);
            this.socket.BeginSend(b, 0, b.Length, 0, new AsyncCallback(this.sendCallback), this.socket);
        }

        private void sendCallback(IAsyncResult ar)
        {
            this.socket.EndSend(ar);
            Net.sendDone.Set();
        }

        private void receive()
        {
            this.sb.Clear();
            Net.receiveDone.Reset();
            this.socket.BeginReceive(this.buffer, 0, Net.bufferSize, 0, new AsyncCallback(receiveCallback), this.socket);
        }

        private void receiveCallback(IAsyncResult ar)
        {
            int bytesRead = this.socket.EndReceive(ar);
            if (bytesRead == 0)
            {
                Net.receiveDone.Set();
            }
            else
            {
                this.sb.Append(Encoding.ASCII.GetString(this.buffer, 0, bytesRead));
                if (this.sb.ToString().IndexOf('\0') == -1)
                {
                    this.socket.BeginReceive(this.buffer, 0, Net.bufferSize, 0, new AsyncCallback(this.receiveCallback), this.socket);
                }
                else
                {
                    Net.receiveDone.Set();
                }
            }
        }

        public Net()
        {
            this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this.xml = new Xml();
        }

        public void setDebug(Boolean debug)
        {
            this.debug = debug;
        }

        public void setForm(MainForm form)
        {
            this.form = form;
        }

        public void close()
        {
            if (this.socket.Connected)
            {
                this.socket.Shutdown(SocketShutdown.Both);
                this.socket.Close();
            }
        }

        public void connect(string host, int port, string pass)
        {
            if (host == "" || port == 0 || pass == "")
            {
                this.form.log("specify params");
                return;
            }
            if (this.debug)
            {
                this.form.log("connected");
                return;
            }
            this.form.log("connecting...");
            this.socket.BeginConnect(host, port, new AsyncCallback(this.connectCallback), this.socket);
            connectDone.WaitOne();
            this.form.log("connected");
            this.auth(pass);
        }

        private void connectCallback(IAsyncResult ar)
        {
            this.socket.EndConnect(ar);
            connectDone.Set();
        }

        private void auth(string pass)
        {
            this.form.log("authenticating...");
            this.send(this.xml.authRequest(pass));
            Net.sendDone.WaitOne();
            this.receive();
            Net.receiveDone.WaitOne();
            if (0 == this.xml.parseAuthResponse(this.sb.ToString()))
            {
                this.form.log("authenticated");
            }
            else
            {
                this.form.log("authentication failed");
            }
        }

        private Boolean isSmartPlug(string device)
        {
            string answer = null;
            if (this.debug)
            {
                answer = this.xml.getDeviceFakeResponse();
            }
            else
            {
                this.send(this.xml.getDeviceRequest(device));
                Net.sendDone.WaitOne();
                this.receive();
                Net.receiveDone.WaitOne();
                answer = this.sb.ToString();
            }
            string[] res = this.xml.parseGetDeviceResponse(answer);
            foreach (string attr in res)
            {
                if (attr.Contains("ZBMPlug15"))
                {
                    return true;
                }
            }
            return false;
        }

        public string[] getDevices()
        {
            string answer = null;
            if (this.debug)
            {
                answer = this.xml.getDevicesFakeResponse();
            }
            else
            {
                this.send(this.xml.getDevicesRequest());
                Net.sendDone.WaitOne();
                this.receive();
                Net.receiveDone.WaitOne();
                answer = this.sb.ToString();
            }
            string[] devs = this.xml.parseGetDevicesResponse(answer);
            List<string> devices = new List<string>();
            for (int i = 0; i < devs.Length; i++)
            {
                if (this.isSmartPlug(devs[i]))
                {
                    devices.Add(devs[i]);
                }
            }
            return devices.ToArray();
        }

        public string[] getDeviceInfo(string device)
        {
            string answer = null;
            if (this.debug)
            {
                answer = this.xml.getDeviceInfoFakeResponse();
            }
            else
            {
                this.send(this.xml.getDeviceInfoRequest(device));
                Net.sendDone.WaitOne();
                this.receive();
                Net.receiveDone.WaitOne();
                answer = this.sb.ToString();
            }
            return this.xml.parseGetDeviceInfoResponse(answer);
        }
    }
}