﻿using System;
using System.Xml;
using System.Collections.Generic;

namespace smartenit
{
    class Xml
    {
        /*********** auth ***********/

        public string authRequest(string password)
        {
            return "<zbpPacket>" +
                       "<Object>ZBP_System</Object>" + 
                       "<methodName>Sys_Authenticate</methodName>" +
                       "<Arguments>" + 
                           "<Argument type='string'>" + password + "</Argument>" + 
                       "</Arguments>" +
                   "</zbpPacket>\0";
        }

        public int parseAuthResponse(string answer)
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(answer);
            XmlNodeList list = xml.GetElementsByTagName("Argument");
            if (list.Count == 0)
            {
                throw new Exception("No auth result found in XML");
            }
            return Int32.Parse(list[0].InnerText);
            //string sampleAnswer = "<zbpPacketResponse><Object>ZBP_System</Object><methodName>Sys_Authenticate</methodName><Arguments><Argument type='uchar'>u8Status</Argument></Arguments></zbpPacketResponse>";
        }

        /*********** devices ***********/

        public string getDevicesRequest()
        {
            return "<zbpPacket>" + 
                       "<Object>ZBP_Node</Object>" +
                       "<methodName>Node_GetNumDevices</methodName>" +
                       "<Arguments></Arguments>" +
                   "</zbpPacket>\0";
        }

        public string[] parseGetDevicesResponse(string answer)
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(answer);
            string xPathString = "//zbpPacketResponse/Arguments/Argument[@type='QVector<ushort>']";
            XmlNode xmlNode = xml.DocumentElement.SelectSingleNode(xPathString);
            if (xmlNode == null)
            {
                throw new Exception("List of devices not found");
            }
            return xmlNode.FirstChild.Value.Trim('[', ']').Split(',');
        }

        public string getDevicesFakeResponse()
        {
            return "<zbpPacketResponse>" +
                       "<Object>ZBP_Node</Object>" +
                       "<methodName>Node_GetNumNodes</methodName>" +
                       "<Arguments>" +
                           "<Argument type='uchar'>9</Argument>" +
                           "<Argument type='QVector&lt;ushort>'>[6b9b,0,88ae,1ea6,aa02,dcc1,e8e1,b9f4,34dc]</Argument>" +
                       "</Arguments>" +
                   "</zbpPacketResponse>";
        }

        /*********** device ***********/

        public string getDeviceRequest(string node)
        {
            return "<zbpPacket>" +
                       "<Object>ZBP_Node</Object>" + 
                       "<methodName>Node_GetNode</methodName>" +
                       "<Arguments>" + 
                           "<Argument type='ushort' base='10'>" + node + "</Argument>" +
                       "</Arguments>" +
                   "</zbpPacket>\0";
        }

        public string[] parseGetDeviceResponse(string answer)
        { 
            List<string> res = new List<string>();
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(answer);
            XmlNodeList list = xml.GetElementsByTagName("Argument");
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].FirstChild != null && list[i].FirstChild.Value != null)
                {
                    res.Add(list[i].FirstChild.Value.ToString());
                }
            }
            return res.ToArray();
        }

        public string getDeviceFakeResponse()
        {
            return "<zbpPacketResponse>" +
                       "<Object>ZBP_Node</Object>" +
                       "<methodName>Node_GetNode</methodName>" +
                       "<Arguments>" +
                           "<Argument type='uchar'>0</Argument>" +               //u8status
                           "<Argument type='ushort'>34dc</Argument>" +           //u16DevAdd
                           "<Argument type='qulonglong'>62</Argument>" +         //u64DevAdd
                           "<Argument type='uint'>0</Argument>" +                //u32NodeDesc
                           "<Argument type='string'>X10</Argument>" +            //userDesc
                           "<Argument type='QVector&lt;ushort>'>[]</Argument>" + //neighbors
                           "<Argument type='QByteArray'>[]</Argument>" +         //endpointDesc
                           "<Argument type='string'>ZBMPlug15</Argument>" +      //sModelId
                           "<Argument type='string'>ZBMPlug15 Lamp</Argument>" + //sNodeName
                           "<Argument type='string'>ZBMPlug15.png</Argument>" +  //sPicture
                           "<Argument type='string'>ZBMPlug15</Argument>" +      //sDeviceCat
                           "<Argument type='string'/>" +                         //sParams
                       "</Arguments>" +
                   "</zbpPacketResponse>";
        }

        public string getDeviceInfoRequest(string device)
        {
            return "<zbpPacket>" +
                       "<Object>ZBP_Node</Object>" +
                       "<methodName>Node_ClusterCommand</methodName>" +
                       "<Arguments>" +
                           "<Argument type='uchar' base='10'>146</Argument>" +              //u8Mode
                           "<Argument type='ushort' base='10'>" + device + "</Argument>" +  //u16DestAdd
                           "<Argument type='uchar' base='10'>2</Argument>" +                //u8DestEP
                           "<Argument type='ushort' base='10'>1794</Argument>" +            //u16ClusterID
                           "<Argument type='uchar' base='10'>0</Argument>" +                //u8CmdID
                           "<Argument type='QByteArray' base='10'>[2,0,0,4,0]</Argument>" + //payload
                       "</Arguments>" +
                   "</zbpPacket>\0";
        }

        public string[] parseGetDeviceInfoResponse(string answer)
        {
            List<string> res = new List<string>();
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(answer);
            XmlNodeList list = xml.GetElementsByTagName("Argument");
            return list[6].FirstChild.Value.ToString().Trim('[',']').Split(',');
        }

        public string getDeviceInfoFakeResponse()
        {
            return "<zbpPacketSignal>" +
                       "<signalName>Node_ClusterCmdRsp</signalName>" +
                       "<Arguments>" +
                           "<Argument type='uchar'>b2</Argument>" +
                           "<Argument type='ushort'>ff42</Argument>" +
                           "<Argument type='uchar'>2</Argument>" +
                           "<Argument type='ushort'>702</Argument>" +
                           "<Argument type='uchar'>a</Argument>" +
                           "<Argument type='uchar'>2</Argument>" +
                           "<Argument type='QByteArray' length='15'>[4,0,2,0,0,0,0,0,25,0,0,0,0,46,2]</Argument>" +
                           "<Argument type='uchar'>2</Argument>" +
                       "</Arguments>" +
                   "</zbpPacketSignal>";
        }
    }
}