﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Threading;

namespace GatewayEmulator
{
    public class StateObject
    {
        public Socket workSocket = null;
        public const int bufferSize = 1024;
        public byte[] buffer = new byte[bufferSize];
        public StringBuilder sb = new StringBuilder();
    }

    class Program
    {
        private static string ip;
        private static int port;
        private static Dictionary<string, Dictionary<string, string>> devices;
        private static ManualResetEvent allDone = new ManualResetEvent(false);

        static void Main(string[] args)
        {
            try
            {
                //
                XmlDocument xml = new XmlDocument();
                xml.Load("./config.xml");

                //
                Program.ip = getStringFromXml(xml, "ip");
                Program.port = Int32.Parse(getStringFromXml(xml, "port"));

                //
                Program.devices = new Dictionary<string, Dictionary<string, string>>();
                XmlNodeList list = getNodesListFromXml(xml, "device");
                for (int i = 0; i < list.Count; i++)
                {
                    Dictionary<string, string> device = new Dictionary<string, string>();
                    for (int j = 0; j < list[i].ChildNodes.Count; j++)
                    {
                        device.Add(list[i].ChildNodes[j].Name, list[i].ChildNodes[j].InnerText);
                    }
                    Program.devices.Add(device["id"], device);
                }

                //
                Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                listener.Bind(new IPEndPoint(IPAddress.Parse(Program.ip), Program.port));
                listener.Listen(10);

                //    
                while (true)
                {
                    try
                    {
                        Console.WriteLine("Waiting for a connection...");
                        allDone.Reset();
                        listener.BeginAccept(new AsyncCallback(Program.acceptCallback), listener);
                        allDone.WaitOne();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error: " + e.Message);
                        continue;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
                Console.WriteLine("Press any key to exit");
                Console.Read();
                return;
            }
        }

        private static void acceptCallback(IAsyncResult ar)
        {
            Console.WriteLine("Connection established");
            allDone.Set();
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);
            StateObject state = new StateObject();
            state.workSocket = handler;
            handler.BeginReceive(state.buffer, 0, StateObject.bufferSize, 0, new AsyncCallback(Program.readCallback), state);
        }

        private static void readCallback(IAsyncResult ar)
        {
            StateObject state = (StateObject) ar.AsyncState;
            Socket handler = state.workSocket;
            int bytesRead = handler.EndReceive(ar);

            if (bytesRead == 0)
            {
                handler.BeginReceive(state.buffer, 0, StateObject.bufferSize, 0, new AsyncCallback(Program.readCallback), state);
            }
            else
            {
                String content = String.Empty;
                state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));
                content = state.sb.ToString();
                if (content.IndexOf('\0') == -1)
                {
                    handler.BeginReceive(state.buffer, 0, StateObject.bufferSize, 0, new AsyncCallback(Program.readCallback), state);
                }
                else
                {
                    Console.WriteLine("Text received: {0}", content);
                    state.sb.Clear();
                    string msg = Program.getResponse(content);
                    handler.Send(Encoding.UTF8.GetBytes(msg));
                    Console.WriteLine("Text send : {0}", msg);
                    handler.BeginReceive(state.buffer, 0, StateObject.bufferSize, 0, new AsyncCallback(Program.readCallback), state);
                }
            }
        }

        private static string getStringFromXml(XmlDocument xml, string tag)
        {
            XmlNodeList list = xml.GetElementsByTagName(tag);
            if (list.Count == 0)
            {
                throw new Exception("No element " + tag + " found in XML");
            }
            return list[0].InnerText;
        }

        private static XmlNodeList getNodesListFromXml(XmlDocument xml, string tag)
        {
            XmlNodeList list = xml.GetElementsByTagName(tag);
            if (list.Count == 0)
            {
                throw new Exception("No element " + tag + " found in XML");
            }
            return list;
        }

        private static string getResponse(string request)
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(request);
            string method = getStringFromXml(xml, "methodName");
            string msg = null;
            switch (method)
            { 
                case "Sys_Authenticate":
                    msg = getAuthResponse();
                    break;

                case "Node_GetNumDevices":
                    msg = getDevicesResponse();
                    break;

                case "Node_GetNode":
                    msg = getDeviceResponse(xml);
                    break;

                case "Node_ClusterCommand":
                    msg = getCommandResponse(xml);
                    break;

                default:
                    throw new Exception("Unknown method found in XML");
            }
            return msg;
        }

        private static string getAuthResponse()
        {
            return "<zbpPacketResponse>" +
                       "<Object>ZBP_System</Object>" +
                       "<methodName>Sys_Authenticate</methodName>" +
                       "<Arguments>" +
                           "<Argument type='uchar'>0</Argument>" +
                       "</Arguments>" +
                   "</zbpPacketResponse>\0";
        }

        private static string getDevicesResponse()
        {
            return "<zbpPacketResponse>" +
                       "<Object>ZBP_Node</Object>" +
                       "<methodName>Node_GetNumNodes</methodName>" +
                       "<Arguments>" +
                           "<Argument type='uchar'>9</Argument>" +
                           "<Argument type='QVector&lt;ushort>'>[" + String.Join(",", Program.devices.Keys) + "]</Argument>" +
                       "</Arguments>" +
                   "</zbpPacketResponse>\0";
        }

        private static string getDeviceResponse(XmlDocument request)
        {
            XmlNodeList list = getNodesListFromXml(request, "Argument");
            Dictionary<string, string> device = Program.devices[list[0].InnerText];
            return "<zbpPacketResponse>" +
                    "<Object>ZBP_Node</Object>" +
                    "<methodName>Node_GetNode</methodName>" +
                    "<Arguments>" +
                        "<Argument type='uchar'>0</Argument>" +               //u8status
                        "<Argument type='ushort'>" + device["id"] + "</Argument>" +           //u16DevAdd
                        "<Argument type='qulonglong'>62</Argument>" +         //u64DevAdd
                        "<Argument type='uint'>0</Argument>" +                //u32NodeDesc
                        "<Argument type='string'>desc</Argument>" +           //userDesc
                        "<Argument type='QVector&lt;ushort>'>[]</Argument>" + //neighbors
                        "<Argument type='QByteArray'>[]</Argument>" +         //endpointDesc
                        "<Argument type='string'>" + device["model"] + "</Argument>" + //sModelId
                        "<Argument type='string'>" + device["name"] + "</Argument>" +  //sNodeName
                        "<Argument type='string'>picture.png</Argument>" +             //sPicture
                        "<Argument type='string'>" + device["type"] + "</Argument>" +  //sDeviceCat
                        "<Argument type='string'/>" +                                  //sParams
                    "</Arguments>" +
                "</zbpPacketResponse>\0";
        }

        private static string getCommandResponse(XmlDocument request)
        {
            XmlNodeList list = getNodesListFromXml(request, "Argument");
            Dictionary<string, string> device = Program.devices[list[1].InnerText];
            Random r = new Random();
            int[] data = new int[15];
            for (int i = 0; i < 15; i++)
            {
                data[i] = r.Next(100);
            }
            return "<zbpPacketSignal>" +
                    "<signalName>Node_ClusterCmdRsp</signalName>" +
                    "<Arguments>" +
                        "<Argument type='uchar'>b2</Argument>" +
                        "<Argument type='ushort'>" + device["id"] + "</Argument>" +
                        "<Argument type='uchar'>2</Argument>" +
                        "<Argument type='ushort'>702</Argument>" +
                        "<Argument type='uchar'>a</Argument>" +
                        "<Argument type='uchar'>2</Argument>" +
                        "<Argument type='QByteArray' length='15'>[" + String.Join(",", data) + "]</Argument>" +
                        "<Argument type='uchar'>2</Argument>" +
                    "</Arguments>" +
                "</zbpPacketSignal>\0";
        }
    }
}