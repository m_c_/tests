<?php

/**
 * 
 */
class ProgException extends Exception
{
}

class Db
{
    /**
     * 
     */
    protected $_people = array();
    
    /**
     * 
     */
    protected $_filename = null;
    
    /**
     * 
     */
    protected $_indexes = array(
        'lastName' => null,
        'birthday' => null
    );
    
    /**
     * 
     */
    public function __construct($filename)
    {
        if (empty($filename) || !is_file($filename)) {
            throw ProgException('No file name given or it\'s not a file');
        }
        $handle = fopen($filename, 'r');
        if ($handle === false) {
            throw ProgException('Cannot open file');
        }
        $this->_indexes['lastName'] = new Tree();
        $this->_indexes['birthday'] = new Tree();
        while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
            if (count($data) == 3) {
                $this->insert($data[0], $data[1], $data[2]);
            }
            else {
                throw ProgException('Wrong data in file, must be: lastName,firstName,birthday\n');
            }
        }
        fclose($handle);
        $this->_filename = $filename;
    }

    /**
     * 
     */
    public function insert($lastName, $firstName, $birthday)
    {
        $p = new Person($lastName, $firstName, $birthday);
        $this->_people[$p->getId()] = $p;

        $this->_indexes['lastName']->add($p->getLastName(), $p->getId());
        $this->_indexes['birthday']->add($p->getBirthday(), $p->getId());

        return $this;
    }

    /**
     *
     */
    public function delete($people)
    {
        if ($people instanceof Person) {
            $people = array($people);
        }
        foreach ($people as $person) {
            unset($this->_people[$person->getId()]);
            $this->_indexes['lastName']->remove($person->getLastName(), $person->getId());
            $this->_indexes['birthday']->remove($person->getBirthday(), $person->getId());
        }
        return $this;
    }

    /**
     * 
     */
    public function select($params = null)
    {
        if (empty($params)) {
            return $this->_people;
        }
        $res = array();
        $ids = array();
        if (isset($params['lastName'])) {
            $ids = $this->_searchIndex($this->_indexes['lastName'], $params['lastName']);
        }
        if (isset($params['birthday'])) {
            $ids = $this->_searchIndex($this->_indexes['birthday'], $params['birthday'], $ids);
        }
        if (isset($params['firstName'])) {
            foreach ($this->_people as $person) {
                $id = $person->getId();
                if ($person->getFirstName() == $params['firstName'] && in_array($id, $ids)) {
                    $ids[] = $id;
                }
            }
        }
        foreach ($ids as $id) {
            $res[] = clone $this->_people[$id];
        }
        return $res;
    }

    /**
     *
     */
    public function update($people)
    {
        if ($people instanceof Person) {
            $people = array($people);
        }
        foreach ($people as $person) {
            $id = $person->getId();
            if (isset($this->_people[$id])) {
                //
                if ($this->_people[$id]->getLastName() != $person->getLastName()) {
                    $this->_indexes['lastName']->remove($this->_people[$id]->getLastName(), $id);
                    $this->_indexes['lastName']->add($person->getLastName(), $id);
                }
                //
                if ($this->_people[$id]->getBirthday() != $person->getBirthday()) {
                    $this->_indexes['birthday']->remove($this->_people[$id]->getBirthday(), $id);
                    $this->_indexes['birthday']->add($person->getBirthday(), $id);
                }
                $this->_people[$id] = $person;
            }
        }
        return $this;
    }
    
    /**
     * 
     */
    public function getCount()
    {
        return count($this->_people);
    }
    
    /**
     * 
     */
    public function save()
    {
        file_put_contents($this->_filename, '');
        foreach ($this->_people as $person) {
            file_put_contents(
                $this->_filename,
                "{$person->getLastName()},{$person->getFirstName()},{$person->getBirthday()}\n",
                FILE_APPEND
            );
        }
    }

    /**
     * 
     */
    public function saveIndexes()
    {
        $filename = './lastNames.txt';
        file_put_contents($filename, '');
        $this->_indexes['lastName']->postOrderSave($filename);

        $filename = './birthdays.txt';
        file_put_contents($filename, '');
        $this->_indexes['birthday']->breadthFirstSave('./birthdays.txt');
    }

    /**
     * 
     */
    public function printIndexes()
    {
        print_r($this->_indexes);
    }

    /**
     * 
     */
    protected function _searchIndex($indexTree, $param, $excludeIds = array())
    {
        return array_diff($indexTree->search($param), $excludeIds);
    }
}

/**
 * 
 */
class Person
{
    protected $_uuid;
    protected $_lastName;
    protected $_firstName;
    protected $_birthday;

    /**
     * 
     */
    public function __construct($lastName, $firstName, $birthday)
    {
        $this->update($lastName, $firstName, $birthday);
        $this->_uuid = uniqid();
    }

    /**
     *
     */
    public function update($lastName = null, $firstName = null, $birthday = null)
    {
        //check
        if (!empty($lastName)) {
            $this->_lastName = $lastName;
        }
        if (!empty($firstName)) {
            $this->_firstName = $firstName;
        }
        if (!empty($birthday)) {
            $this->_birthday = $birthday;
        }
    }

    /**
     * 
     */
    public function toString()
    {
        return "{$this->_firstName} {$this->_lastName}, born {$this->_birthday}";
    }
    
    /**
     *
     */
    public function getId()
    {
        return $this->_uuid;
    }
    
    /**
     * 
     */
    public function getLastName()
    {
        return $this->_lastName;
    }

    /**
     * 
     */
    public function getFirstName()
    {
        return $this->_firstName;
    }

    /**
     * 
     */
    public function getBirthday()
    {
        return $this->_birthday;
    }
}

/**
 *
 */
class Tree
{
    /**
     * 
     */
    protected $_root = null;
    
    /**
     * 
     */
    public function add($key, $data)
    {
        $n = new Node($key, $data);
        $this->_root = $this->_insert($this->_root, $n);
        return $this;
    }
    
    /**
     *
     */
    public function remove($key, $data)
    {
        foreach ($this->search($key, true) as $node) {
            if ($node->getData() == $data) {
                $this->_delete($node);
            }
        }
        return $this;
    }

    /**
     * 
     */
    public function search($key, $nodes = false)
    {
        $node = $this->_root;
        $res  = array();
        while (!is_null($node)) {
            if ($node->getKey() == $key) {
                $res[] = $nodes ? $node : $node->getData();
            }
            if ($node->getKey() > $key) {
                $node = $node->getLeft();
            }
            else {
                $node = $node->getRight();
            }
        }
        return $res;
    }
    
    /**
     * 
     */
    public function postOrderSave($filename)
    {
        if (empty($filename)) {
            throw new ProgException('Provide filename');
        }
        $this->_postOrderSave($this->_root, $filename);
    }

    /**
     *
     */
    public function breadthFirstSave($filename)
    {
        if (empty($filename)) {
            throw new ProgException('Provide filename');
        }
        $q = new SplQueue();
        $q->enqueue($this->_root);
        while (!$q->isEmpty()) {
            $node = $q->dequeue();
            file_put_contents($filename, $node->getKey() . "\n", FILE_APPEND);
            if (!is_null($node->getLeft())) {
                $q->enqueue($node->getLeft());
            }
            if (!is_null($node->getRight())) {
                $q->enqueue($node->getRight());
            }
        }
    }

    /**
     * 
     */
    protected function _getMin(Node $node)
    {
        $curr = $node;
        while (!is_null($curr->getLeft())) {
            $curr = $curr->getLeft();
        }
        return $curr;
    }
    
    /**
     * 
     */
    protected function _insert($node, Node $newNode)
    {
        if (is_null($node)) {
            return $newNode;
        }
        if ($node->getKey() > $newNode->getKey()) {
            $node->setLeft($this->_insert($node->getLeft(), $newNode));
        }
        else {
            $node->setRight($this->_insert($node->getRight(), $newNode));
        }
        return $node;
    }

    /**
     *
     */
    protected function _delete(Node $node)
    {
        if (is_null($node)) {
            return;
        }
        $hasLeft  = !is_null($node->getLeft());
        $hasRight = !is_null($node->getRight());
        $parent   = $node->getParent();
        if (!$hasLeft && !$hasRight) {
            $parent->replaceChild($node, null);
            unset($node);
        }
        else {
            if ($hasLeft && !$hasRight) {
                $parent->replaceChild($node, $node->getLeft());
                $node->setLeft(null);
                unset($node);
            }
            else if (!$hasLeft && $hasRight) {
                $parent->replaceChild($node, $node->getRight());
                $node->setRight(null);
                unset($node);
            }
            else {
                $newNode = $this->_getMin($node->getRight());
                $node->setKey($newNode->getKey());
                $node->setData($newNode->getData());
                $this->_delete($newNode);
            }
        }
    }
    
    /**
     *
     */
    protected function _postOrderSave($node, $filename)
    {
        if (is_null($node)) {
            return;
        }
        $this->_postOrderSave($node->getLeft(), $filename);
        $this->_postOrderSave($node->getRight(), $filename);
        file_put_contents($filename, $node->getKey() . "\n", FILE_APPEND);
    }
}

/**
 * 
 */
class Node
{
    /**
     *
     */
    protected $_key = null;

    /**
     * 
     */
    protected $_data = null;

    /**
     *
     */
    protected $_left = null;
    
    /**
     *
     */
    protected $_right = null;

    /**
     *
     */
    protected $_parent = null;

    /**
     * 
     */
    public function __construct($key, $data)
    {
        if (empty($key) || empty($data)) {
            throw new ProgException('Key and data must not be empty');
        }
        $this->_key  = $key;
        $this->_data = $data;
    }
    
    /**
     * 
     */
    public function getKey()
    {
       return $this->_key; 
    }

    /**
     *
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     *
     */
    public function getLeft()
    {
        return $this->_left;
    }

    /**
     *
     */
    public function getRight()
    {
        return $this->_right;
    }

    /**
     *
     */
    public function setKey($key)
    {
        $this->_key = $key;
        return $this;
    }
    
    /**
     *
     */
    public function setData($data)
    {
        $this->_data = $data;
        return $this;
    }
    
    /**
     *
     */
    public function setLeft($left)
    {
        $this->_left = $left;
        if (!is_null($left)) {
            $left->setParent($this);
        }
        return $this;
    }
    
    /**
     *
     */
    public function setRight($right)
    {
        $this->_right = $right;
        if (!is_null($right)) {
            $right->setParent($this);
        }
        return $this;
    }
    
    /**
     *
     */
    public function getParent()
    {
        return $this->_parent;
    }
    
    /**
     *
     */
    public function setParent($parent)
    {
        $this->_parent = $parent;
        return $this;
    }
    
    /**
     * 
     */
    public function replaceChild(Node $oldChild, $newChild)
    {
        if ($this->getLeft() == $oldChild) {
            $this->setLeft($newChild);
        }
        else {
            $this->setRight($newChild);
        }
        if (!is_null($newChild)) {
            $newChild->setParent($this);
        }
        $oldChild->setParent(null);
        return $this;
    }
}

/**
 * 
 */
function generateDB($filename)
{
    if (empty($filename)) {
        throw ProgException('No file name given');
    }
    $last = array('First', 'Second', 'Third', 'Fourth', 'Fifth', 'Sixth', 'Seventh', 'Eighth', 'Nineth', 'Tenth');
    $first = 'John';
    file_put_contents($filename, '');
    foreach ($last as $l) {
        $date = rand(1950, 2017) . '-' .
                str_pad(rand(0, 12), 2, '0', STR_PAD_LEFT) . '-' .
                str_pad(rand(0, 29), 2, '0', STR_PAD_LEFT);
        $person = "$l,$first,$date\n";
        file_put_contents($filename, $person, FILE_APPEND);
    }
}

/**
 * 
 */
function test($filename)
{
    $db = new Db($filename);
    //
    echo "count: ", $db->getCount(), "\n";
    //
    echo "select Fifth: \n";
    foreach ($db->select(array('lastName' => 'Fifth')) as $p) {
        echo $p->toString(),"\n";
    }
    //
    $db->printIndexes();
    //
    echo "insert\n";
    $db->insert('Eleventh', 'John', '1980-01-01');
    //
    echo "count: ", $db->getCount(), "\n";
    //
    $db->printIndexes();
    //
    echo "delete\n";
    $p = $db->select(array('lastName' => 'Eleventh'));
    if (!empty($p)) {
        $db->delete($p[0]);
    }
    //
    echo "count: ", $db->getCount(), "\n";
    //
    $db->printIndexes();
    //
    echo "update\n";
    $p = $db->select(array('lastName' => 'Sixth'));
    if (!empty($p)) {
        $p[0]->update(null, 'Jack', '1999-01-02');
        $db->update($p[0]);
    }
    //
    $db->printIndexes();
    //
    echo "all:\n";
    foreach ($db->select() as $p) {
        echo $p->toString(),"\n";
    }
}

//*******************************************
$dbFilename = './data.csv';
//generateDB($dbFilename);
//test($dbFilename);
$db = new Db($dbFilename);
$db->saveIndexes();