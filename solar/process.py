# 1.0 First, we import pvlib’s objects, module data, and inverter data.
import pandas as pd
import pvlib
from pvlib.pvsystem import PVSystem
from pvlib.location import Location
from pvlib.modelchain import ModelChain
import plotly


def plot(params):
    ''' '''

    data = run(params)
    x = data.axes[0]
    lines = []
    for ax in data.axes[1]:
        lines.append(
            plotly.graph_objs.Scatter(
                x=x,
                y=data[ax],
                mode='markers',
                name=ax
            )
        )
    return plotly.offline.plot(lines, show_link=False, output_type='div')


def run(params):
    ''' '''

    # load some module and inverter specifications
    sandia_modules = pvlib.pvsystem.retrieve_sam('SandiaMod')
    cec_inverters = pvlib.pvsystem.retrieve_sam('cecinverter')
    sandia_module = sandia_modules['Canadian_Solar_CS5P_220M___2009_']
    cec_inverter = cec_inverters['ABB__MICRO_0_25_I_OUTD_US_208_208V__CEC_2014_']

    # 2.0 we create a Location object, a PVSystem object, and a ModelChain object
    # can put timezone after altitude, tz=
    location = Location(32.2, -110.9, altitude=100)

    # input racking model as well under : rackikng_model
    system = PVSystem(
        surface_tilt=20,
        surface_azimuth=200,
        module_parameters=sandia_module,
        inverter_parameters=cec_inverter,
        albedo=3.0
    )
    mc = ModelChain(
        system,
        location,
        airmass_model=params['airmass'],
        clearsky_model=params['clearsky']
    )
    # can also add
    # orientation_strategy: None;  transposition_model: haydavies ; solar_position_method: nrel_numpy
    # dc_model: singlediode
    # ac_model: snlinverter
    # aoi_model: physical_aoi_loss
    # spectral_model: no_spectral_loss
    # temp_model: sapm_temp
    # losses_model: no_extra_losses

    # 3.0 Printing a ModelChain object will display its models
    # print(mc)

    # 4.0 we run a model with some simple weather data
    weather = pd.DataFrame(
        [[1050, 1000, 100, 30, 5]],
        columns=['ghi', 'dni', 'dhi', 'temp_air', 'wind_speed'],
        index=[
            pd.Timestamp('20170401120000', tz='US/Arizona'),
            pd.Timestamp('20170402120000', tz='US/Arizona'),
            pd.Timestamp('20170403120000', tz='US/Arizona'),
            pd.Timestamp('20170404120000', tz='US/Arizona')
        ]
    )
    mc.run_model(times=weather.index, weather=weather)
    print(mc.dc.to_dict())
    return mc.dc
    # 5.0
    # print(mc.aoi)

    # 6.0
    # print(mc.dc)

    # 7.0
    # print(mc.ac)
