from flask import Flask, request, render_template
from form import ModelingForm
from process import plot


DEBUG = True
SECRET_KEY = 'not very secret developer\'s key'
app = Flask(__name__)
app.config.from_object(__name__)


@app.route('/')
def home_view():
    ''' '''
    return render_template('index.html', form=ModelingForm())


@app.route('/process', methods=['POST'])
def process():
    ''' '''
    form = ModelingForm(request.form)
    image = None
    if form.validate():
        image = plot(form.data)
    return render_template('process.html', form=form, image=image)


if __name__ == '__main__':
    app.run()
