from wtforms import Form, fields, widgets
from wtforms.fields import html5


class ModelingForm(Form):
    ''' '''

    #fromDate = html5.DateField('fromDate')
    #toDate = html5.DateField('toDate')
    #longitude = html5.DecimalField('longitude', places=4)
    #latitude = html5.DecimalField('latitude', places=4)
    airmass = fields.SelectField(
        'Air mass model', choices=[('kastenyoung1989','kastenyoung1989')]
    )
    clearsky = fields.SelectField(
        'Clear sky model', choices=[('ineichen','ineichen')]
    )
    