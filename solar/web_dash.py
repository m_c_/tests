'''
;dash==0.19.0
;dash-core-components==0.15.0
;dash-html-components==0.8.0
;dash-renderer==0.11.1
'''

import dash
import dash_core_components as dcc
import dash_html_components as dhc
from dash.dependencies import Output, Input
import plotly.graph_objs as go

from process import run


def _create_app():
    ''' Creates dash application '''

    app = dash.Dash()
    app.layout = dhc.Div(children=[
        dcc.Dropdown(
            id='airmass',
            options=[{'label': 'kastenyoung1989', 'value': 'kastenyoung1989'}]
        ),
        dcc.Dropdown(
            id='clearsky',
            options=[{'label': 'ineichen', 'value': 'ineichen'}]
        ),
        dhc.Button('Process', id='process'),
        dcc.Graph(
            id='main-graph',
            figure={
                'data': _get_data(),
                'layout': _get_layout()
            }
        )
    ])

    @app.callback(
        Output('main-graph', 'figure'),
        [
            Input('airmass', 'value'),
            Input('clearsky', 'value'),
            Input('process', 'n_clicks')
        ]
    )
    def update_main_graph(airmass, clearsky, clck):
        ''' '''
        return _get_data({
            'airmass': airmass, 'clearsky': clearsky
        })

    return app


def _get_data(params=None):
    ''' '''

    if params is None:
        return []

    data = run(params)
    x = data.axes[0]
    '''
    [{
        'name': 'Some name',
        'mode': 'line',
        'line': {
            'color': 'rgb(0, 0, 0)',
            'opacity': 1
        },
        'type': 'scatter',
        'x': [randint(1, 100) for x in xrange(0, 20)],
        'y': [randint(1, 100) for x in xrange(0, 20)]
    }]
    '''
    lines = []
    for ax in data.axes[1]:
        lines.append(
            go.Scatter(
                x=x,
                y=data[ax],
                mode='markers',
                name=ax
            )
        )
    return lines


def _get_layout():
    ''' '''
    return {
        'autosize': True,
        'margin': {
            'l': 0,
            'r': 0,
            't': 0,
            'b': 0
        },
        'scene': {
            'bgcolor': 'rgb(255, 255, 255)',
            'xaxis': {
                'titlefont': {'color': 'rgb(0, 0, 0)'},
                'title': 'Date',
                'color': 'rgb(0, 0, 0)',
            },
            'yaxis': {
                'titlefont': {'color': 'rgb(0, 0, 0)'},
                'title': 'Value',
                'color': 'rgb(0, 0, 0)'
            }
        }
    }


if __name__ == '__main__':
    app = _create_app()
    app.run_server()
