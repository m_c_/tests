from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from telegram import constants
import requests
import logging


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

token = ''

def chunkstring(string, length):
    ''' '''
    return (string[0 + i:length + i] for i in range(0, len(string), length))

# api

server = 'https://owlsight.com'
base_url = server + ''
auth_url = server + ''
supported = (
    'auth',
    'version',
    'api groups-get'
)

def auth(user, passw):
    ''' '''
    try:
        res = requests.post(auth_url, data={'m': user, 'p': passw})
        if res.status_code == 200:
            return res.cookies, None
        else:
            return None, 'Error getting answer from server, HTTP code ' + str(res.status_code)
    except Exception as e:
        logger.error('Login caused error "%s"'.format(e))
        return None, 'Internal error'

def api(method, cookies=None, params=None):
    ''' '''
    try:
        url = '{}/{}'.format(base_url, method)
        if params:
            res = requests.post(url, data=params, cookies=cookies)
        else:
            res = requests.get(url, cookies=cookies)
        if res.status_code == 200:
            return res.text
        else:
            return 'Error getting answer from server, HTTP code ' + str(res.status_code)
    except Exception as e:
        logger.error('Call to API "%s" caused error "%s"'.format(method, e))
        return 'Internal error'
        

# handlers

def reply(update, context, txt):
    ''' '''
    context.bot.send_message(chat_id=update.message.chat_id, text=txt)

def text(update, context):
    ''' '''
    reply(update, context, 'This bot can\'t talk, only obey commands')

def error(update, context):
    ''' '''
    logger.error('Update "%s" caused error "%s"', update, context.error)
    reply(update, context, 'Internal error')

def start(update, context):
    ''' '''
    reply(update, context, 'I\'m OwlsightApi bot, command me')

def help(update, context):
    ''' '''
    reply(update, context, 'Commands you can use:\n' + '\n'.join(['/{}'.format(s) for s in supported]))

def api_h(update, context):
    ''' '''
    if len(context.args) > 0:
        if 'owl_cookie' in context.user_data:
            txt = api(context.args[0], context.user_data['owl_cookie'])
            if len(txt) > constants.MAX_MESSAGE_LENGTH:
                for chunk in chunkstring(txt, constants.MAX_MESSAGE_LENGTH):
                    reply(update, context, chunk)
            else:
                reply(update, context, txt)
        else:
            reply(update, context, 'You have to /auth first')
    else:
        reply(update, context, 'Provide at least method name')

def version(update, context):
    ''' '''
    reply(update, context, api('version'))

def auth_h(update, context):
    ''' '''
    if len(context.args) == 2:
        cookie, res = auth(context.args[0], context.args[1])
        if cookie:
            context.user_data['owl_cookie'] = cookie
            reply(update, context, 'Authorized, now you can make API requests')
        else:
            reply(update, context, 'Error auth to server')
    else:
        reply(update, context, 'Provide email and password')

# main

def main():
    ''' '''
    updater = Updater(token=token, use_context=True)
    dispatcher = updater.dispatcher

    dispatcher.add_error_handler(error)

    text_handler = MessageHandler(Filters.text, text)
    dispatcher.add_handler(text_handler)

    start_handler = CommandHandler('start', start)
    dispatcher.add_handler(start_handler)

    help_handler = CommandHandler('help', help)
    dispatcher.add_handler(help_handler)

    version_handler = CommandHandler('version', version)
    dispatcher.add_handler(version_handler)

    auth_handler = CommandHandler('auth', auth_h)
    dispatcher.add_handler(auth_handler)

    api_handler = CommandHandler('api', api_h)
    dispatcher.add_handler(api_handler)

    updater.start_polling()


if __name__ == '__main__':
    main()
