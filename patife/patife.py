# -*- coding: utf-8 -*-
from __builtin__ import unicode
from flask import Flask, request, session, redirect, url_for, abort, \
    render_template, flash, _app_ctx_stack  # Import Flask libraries and features
from datetime import datetime, date, time
from sqlalchemy import create_engine, Column, Integer, String, Date, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# Configuration for our Flask application
DEBUG = True
SECRET_KEY = 'development key'
USERNAME = 'admin'
PASSWORD = 'default'

# Configuration for DB
MYSQL_DATABASE_HOST = '127.0.0.1'
MYSQL_DATABASE_DB = 'test'
MYSQL_DATABASE_USER = 'root'
MYSQL_DATABASE_PASSWORD = ''

# Creating the application
app = Flask(__name__)
# Configuring the application using default naming
app.config.from_object(__name__)

Base = declarative_base()

class Entry(Base):
    __tablename__ = 'entries'

    id = Column(Integer, primary_key=True)
    title_en = Column(String(255), nullable=False)
    title_pt = Column(String(255), nullable=False)
    text_en = Column(Text, nullable=False)
    text_pt = Column(Text, nullable=False)
    date_created = Column(Date, nullable=False)
    date_updated = Column(Date, nullable=False)
    category_id = Column(Integer, nullable=True)

class Category(Base):
    __tablename__ = 'categories'

    id = Column(Integer, primary_key=True)
    title_en = Column(String(255), nullable=False)
    title_pt = Column(String(255), nullable=False)
    weight = Column(Integer, nullable=False)

def get_db():
    """
    Opens a new database connection if there is none yet for the
    current application context.
    """
    top = _app_ctx_stack.top  # Get top element in application context stack
    if not hasattr(top, 'mysql_db'):  # If we have not connected to DB - connect
        Session = sessionmaker()
        engine = create_engine(
            'mysql+pymysql://%s:%s@%s/%s' % (
                MYSQL_DATABASE_USER, MYSQL_DATABASE_PASSWORD, MYSQL_DATABASE_HOST, MYSQL_DATABASE_DB
            )
        )
        Session.configure(bind=engine)
        top.mysql_db = Session()
        #top.mysql_db = mysql.connect()
    return top.mysql_db


def init_db():
    """Creates the database tables."""
    with app.app_context():
        Base.metadata.create_all(get_db().get_bind())


@app.teardown_appcontext
def close_db_connection(exception):
    """Closes the database again at the end of the request."""
    top = _app_ctx_stack.top  # Get top element in application context stack
    # If we have a connection to DB - close it
    if hasattr(top, 'mysql_db'):
        top.mysql_db.close()


@app.route('/')
def home_view():
    return redirect(url_for('view_entries'))


@app.route('/entries/')
def view_entries():
    dbs = get_db()
    entries = dbs.query(Entry).order_by(Entry.date_created.desc())
    categories = dbs.query(Category).order_by(Category.weight.desc())
    # Rendering index page
    return render_template('entry_read_all.html', entries=entries, categories=categories)


@app.route('/entries/<int:id>/')
def view_entry(id):
    entry = get_db().query(Entry).get(id)
    if entry is None:
        abort(401)
    # Rendering edit page
    return render_template('entry_read.html', entry=entry)


@app.route('/entries/new')
def new_entry():
    if not session.get('logged_in'):
        abort(401)

    categories = get_db().query(Category).order_by(Category.weight.desc())
    # just displaying the blank template to add
    return render_template('entry_create.html', categories=categories, current_time=date.today())


@app.route('/entries/add', methods=['POST'])
def add_entry():
    # If we are not logged in - abort request
    if not session.get('logged_in'):
        abort(401)

    e = Entry(
        title_en = unicode(request.form['title_en']),
        text_en = unicode(request.form['text_en']),
        title_pt = unicode(request.form['title_pt']),
        text_pt = unicode(request.form['text_pt']),
        date_created = request.form['date_created'],
        date_updated = request.form['date_updated'],
        category_id = request.form['category_id']
    )
    dbs = get_db()
    dbs.add(e)
    dbs.commit()
    # Return index page with successful message
    flash('New entry was successfully posted')
    return redirect(url_for('home_view'))


@app.route('/entries/<int:id>/edit')
def edit_entry(id):
    if not session.get('logged_in'):
        abort(401)

    dbs = get_db()
    entry = dbs.query(Entry).get(id)
    categories = dbs.query(Category).order_by(Category.weight.desc())
    # Rendering edit page
    return render_template(
        'entry_update_and_delete.html', entry=entry, categories=categories, current_time=date.today()
    )


@app.route('/entries/update', methods=['POST'])
def update_entry():
    # If we are not logged in - abort request
    if not session.get('logged_in'):
        abort(401)

    dbs = get_db()
    e = dbs.query(Entry).get(request.form['id'])
    # If we have not record with this ID - return 404 error
    if e is not None:
        e.title_en = unicode(request.form['title_en']),
        e.text_en = unicode(request.form['text_en']),
        e.title_pt = unicode(request.form['title_pt']),
        e.text_pt = unicode(request.form['text_pt']),
        e.date_created = request.form['date_created'],
        e.date_updated = request.form['date_updated'],
        e.category_id = request.form['category_id']
        dbs.commit()
        # Return index page with successful message
        flash('Entry was successfully updated')
        return redirect(url_for('view_entries'))
    else:
        abort(404)


@app.route('/entries/delete', methods=['POST'])
def delete_entry():
    # If we are not logged in - abort request
    if not session.get('logged_in'):
        abort(401)

    dbs = get_db()
    e = dbs.query(Entry).get(request.form['id'])
    # If we have not record with this ID - return 404 error
    if e is not None:
        dbs.delete(e)
        dbs.commit()
        # Return index page with successful message
        flash('Entry was successfully deleted')
        return redirect(url_for('home_view'))
    else:
        abort(404)


@app.route('/categories/')
def view_categories():
    categories = get_db().query(Category).order_by(Category.weight.desc())
    # Rendering index page
    return render_template('category_read_all.html', categories=categories)


@app.route('/categories/<int:id>/')
def view_category(id):
    category = get_db().query(Category).get(id)
    if category is None:
        abort(401)

    entries = get_db().query(Entry).filter(Entry.category_id==id)
    # Rendering edit page
    return render_template('category_read.html', category=category, entries=entries)


@app.route('/categories/new')
def new_category():
    if not session.get('logged_in'):
        abort(401)

    categories = get_db().query(Category).order_by(Category.weight.desc())  
    # just displaying the blank template to add
    return render_template('category_create.html', categories=categories)


@app.route('/categories/add', methods=['POST'])
def add_category():
    # If we are not logged in - abort request
    if not session.get('logged_in'):
        abort(401)

    c = Category(
        title_en = unicode(request.form['title_en']),
        title_pt = unicode(request.form['title_pt']),
        weight = request.form['weight']
    )
    dbs = get_db()
    dbs.add(c)
    dbs.commit()
    # Return index page with successful message
    flash('New category was successfully posted')
    return redirect(url_for('view_categories'))


@app.route('/categories/<int:id>/edit')
def edit_category(id):
    if not session.get('logged_in'):
        abort(401)

    category = get_db().query(Category).get(id)
    # Rendering edit page
    return render_template('category_update_and_delete.html', category=category)


@app.route('/categories/update', methods=['POST'])
def update_category():
    # If we are not logged in - abort request
    if not session.get('logged_in'):
        abort(401)

    dbs = get_db()
    c = dbs.query(Category).get(request.form['id'])
    # If we have not record with this ID - return 404 error
    if c is not None:
        c.title_en = unicode(request.form['title_en'])
        c.title_pt = unicode(request.form['title_pt'])
        c.weight = request.form['weight']
        dbs.commit()
        # Return index page with successful message
        flash('Category was successfully updated')
        return redirect(url_for('view_categories'))
    else:
        abort(404)


@app.route('/categories/delete', methods=['POST'])
def delete_category():
    # If we are not logged in - abort request
    if not session.get('logged_in'):
        abort(401)

    dbs = get_db()
    c = dbs.query(Category).get(request.form['id'])
    # If we have not record with this ID - return 404 error
    if c is not None:
        es = dbs.query(Entry).filter(Entry.category_id==c.id)
        dbs.delete(c)
        for e in es:
            e.category_id = None
        dbs.commit()
        # success message
        flash('Category was successfully deleted and related Entries updated')
        return redirect(url_for('home_view'))
    else:
        abort(404)


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    # If we made POST request - check username and password
    # Else - return login page
    if request.method == 'POST':
        # First check username, then password
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            # If correct - set True to session in browser
            session['logged_in'] = True
            # Return index page with successful message
            flash('Logged in')
            return redirect(url_for('home_view'))
    return render_template('login_view.html', error=error)


@app.route('/logout')
def logout():
    # Delete logged_in option from session in browser
    session.pop('logged_in', None)
    # Delete logged_in option from session in browser
    flash('See you buddy! You were logged out')
    return redirect(url_for('home_view'))


@app.route('/config_db')
def config_db():
    # call this function to reset the whole database. eheheheh
    # If we are not logged in - abort request
    if not session.get('logged_in'):
        abort(401)
    init_db()
    return redirect(url_for('home_view'))


@app.route('/test')
def test_css():
    #test CSS
    return render_template('CSSplayground.html')

if __name__ == '__main__':
    # un-comment the init_db below if you want to start without any db setup.
    # init_db()
    app.run()  # Execute app
