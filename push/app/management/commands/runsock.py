import logging
from tornado import web, ioloop
from sockjs.tornado import SockJSRouter, SockJSConnection
from django.core.management.base import BaseCommand
from push.settings import SOCKJS_PORT, SOCKJS_PATH
from app.server import Connection


logging.getLogger().setLevel(logging.DEBUG)


class Command(BaseCommand):
    args = ''
    help = 'Starts the sockjs-tornado server for handling sockjs connections'

    def handle(self, *args, **kwargs):
        ''' '''

        router = SockJSRouter(Connection, SOCKJS_PATH)
        app = web.Application(router.urls)
        app.listen(SOCKJS_PORT)
        self.stdout.write('Starting sockjs-tornado server on port %s' % SOCKJS_PORT)
        ioloop.IOLoop.instance().start()