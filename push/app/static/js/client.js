app = {
    socket: null,
    connect: function() {
        app.socket = new SockJS('http://127.0.0.1:8080/main');
        app.socket.onopen = function() {
            console.log('open');
        };
        app.socket.onmessage = function(e) {
            console.log('message', e.data);
        };
    },
    disconnect: function() {
        app.socket.close();
    },
    send: function(message) {
        app.socket.send(message);
    },
    sendMessage: function() {
        var el = document.getElementById('msg');
        app.send(el.value);
    }
}