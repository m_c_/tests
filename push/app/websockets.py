from websocket import create_connection
from push.settings import SOCKJS_PORT, SOCKJS_PATH


def send(msg):
    ''' '''
    print('ws://127.0.0.1:{0}/{1}/websocket'.format(SOCKJS_PORT, SOCKJS_PATH))
    ws = create_connection('ws://127.0.0.1:{0}{1}/websocket'.format(SOCKJS_PORT, SOCKJS_PATH))
    ws.send(msg)
    ws.close()