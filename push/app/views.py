from json import dumps
from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response
from app.websockets import send

def index(request):
    ''' '''
    return render_to_response('index.html', {}, context_instance=RequestContext(request))


def push(request, msg):
    ''' '''
    send(msg)
    return HttpResponse(dumps({
        'success': True
    }))