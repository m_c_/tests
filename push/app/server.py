from sockjs.tornado import SockJSConnection


class Connection(SockJSConnection):
    ''' '''
    participants = set()

    def on_open(self, info):
        '''
        @param info sockjs.tornado.session.ConnectionInfo
        '''
        self.broadcast(self.participants, "Someone joined.")
        self.participants.add(self)
        print('Someone joined')

    def on_message(self, message):
        self.broadcast(self.participants, message)
        print('Message: %s' % message)

    def on_close(self):
        self.participants.remove(self)
        self.broadcast(self.participants, "Someone left.")