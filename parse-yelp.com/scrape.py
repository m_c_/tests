#! python3
'''
Yelp has API for this
'''

from json import loads,dumps
import requests
from bs4 import BeautifulSoup


class SoupException(Exception):
    ''' '''
    pass


def getPages(soup):
    ''' '''
    result = []
    return result


def find(soup, name, attrs):
    '''
    Find element in soup
    @param soup: BeautifulSoup
    @param name: string
    @param attrs: dict
    @return: BeautifulSoup
    '''
    elem = soup.find(name, attrs)
    if elem is None:
        raise SoupException('Element with name {0} and attributes {1} not found'.format(name, str(attrs)))
    return elem


def parseDetailsPage(url):
    '''
    Parse page with details
    @param url: string
    @return: dict with data
    '''
    result = {}

    soup = BeautifulSoup(requests.get(url).text, 'html.parser')

    title = find(soup, 'h1', {'itemprop': 'name', 'class': 'biz-page-title embossed-text-white shortenough'})
    result['title'] = title.string

    details = find(soup, 'div', {'class': 'mapbox-container'})

    address = find(details, 'address', {'itemprop': 'address'})
    result['street'] = find(address, 'span', {'itemprop': 'streetAddress'})
    result['city'] = find(address, 'span', {'itemprop': 'addressLocality'})
    result['state'] = find(address, 'span', {'itemprop': 'addressRegion'})
    result['postalCode'] = find(address, 'span', {'itemprop': 'postalCode'})

    phone = find(details, 'span', {'itemprop': 'telephone', 'class': 'biz-phone'})
    result['phone'] = phone.string

    web = find(find(details, 'span', {'class': 'biz-website'}), 'a')
    #/biz_redir?url=http%3A%2F%2Fwww.liebertassociates.com&src_bizid=7avLC9w8x_DLfPxVvpcv1Q&cachebuster=1470184196&s=adb41ddbd380b6f7b7b0c6a45aea79baab15d9a9e6fd5e5cd8ff43f4db23a42b
    start = web['href'].find('http')
    result['site'] = web['href'][start:].split('&')[0]

    div = find(soup, 'div', {'class': 'showcase-photo-box'})
    img = find(div, 'img', {'class': 'photo-box-img'})
    result['image'] = img['src']

    return result


def parseSearchPage(url):
    '''
    Parse one page of search results to get links to details pages
    @param url: string
    @return: list of hrefs
    '''
    result = []

    soup = BeautifulSoup(requests.get(url).text, 'html.parser')
    for li in soup.find_all('li', attrs={'class': 'regular-search-result'}):
        a = find(li, 'a', {'class': 'biz-name js-analytics-click'})
        result.append('yelp.com{0}'.format(a['href']))

    return result


def main():
    ''' '''
    url = 'http://www.yelp.com/search?find_desc=accounting+firms&find_loc=New+York%2C+NY&ns=1';

    html = requests.get(url).text
    soup = BeautifulSoup(html, 'html.parser')
    pages = getPages(soup)