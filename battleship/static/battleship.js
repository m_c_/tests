const battleConsts = { 
    states: {
        empty: 0,
        ship: 1,
        blownempty: 2,
        blownship: 3
    }
};
const battleArea = Vue.extend({
    props: {
        row: Number,
        column: Number,
        value: Number
    },
    computed: {
        title: function () {
            if (
                this.value == battleConsts.states.blownempty ||
                this.value == battleConsts.states.blownship
            ) {
                return 'X';
            }
            return '';
        },
        cssc: function () {
            if (
                this.value == battleConsts.states.ship ||
                this.value == battleConsts.states.blownship
            ) {
                return 'ship';
            }
            return 'empty';
        }
    },
    methods: {
        click: function(event) {
            this.$emit('clicked', {row: this.row, col: this.column});
        }
    },
    template: '<td v-on:click="click" v-bind:class="cssc">{{ title }}</td>'
});
const battleFieldParent = Vue.extend({
    props: {
        size: Number,
        started: {type: Boolean, default: false},
    },
    data: function() {
        var data = [];
        for (var i = 0; i < this.size; i++) {
            data.push((new Array(this.size)).fill(battleConsts.states.empty));
        }
        return {
            areas: data,
            //size: count
            maxShips: { 1: 5, 2: 4, 3: 3, 4: 2, 5: 1 }
        }
    },
    components: {
        'battlearea': battleArea
    },
    template:
        `<table class="battlefield">
            <template v-for="r in size">
                <tr>
                    <template v-for="c in size">
                        <battlearea v-bind:row="r-1" v-bind:column="c-1" v-bind:value="areas[r-1][c-1]" v-on:clicked="clicked"></battlearea>
                    </template>
                </tr>
            </template>
        </table>`
});
const battleFieldOwn = battleFieldParent.extend({
    methods: {
        clicked: function(data) {
            if (this.started) {
                return;
            }
            var row = data['row'], col = data['col'];
            //
            if (this.areas[row][col] == battleConsts.states.ship) {
                this.clear(row, col);
                Vue.set(this.areas[row], col, battleConsts.states.empty);
                return;
            }
            //
            this.areas[row][col] = battleConsts.states.ship;
            if (this.areShipsInOrder()) {
                Vue.set(this.areas[row], col, battleConsts.states.ship);
            }
            else {
                this.areas[row][col] = battleConsts.states.empty;
            }
        },
        areShipsInOrder: function(final) {
            if (typeof final == 'undefined') {
                final = false;
            }
            var len = 0, ships = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 }, col;
            for (var row = 0; row < this.size; row++) {
                for (col = 0; col < this.size; col++) {
                    if (this.areas[row][col] == battleConsts.states.empty) {
                        continue;
                    }
                    var hasUp = this.hasUp(row, col),
                        hasDown = this.hasDown(row, col),
                        hasLeft = this.hasLeft(row, col),
                        hasRight = this.hasRight(row, col);
                    //check area heighbours
                    if (
                        (hasUp && (hasLeft || hasRight || this.hasHorizontal(row - 1, col))) ||
                        (hasDown && (hasLeft || hasRight || this.hasHorizontal(row + 1, col))) ||
                        (hasLeft && (hasUp || hasDown || this.hasVertical(row, col - 1))) ||
                        (hasRight && (hasUp || hasDown || this.hasVertical(row, col + 1)))
                    ) {
                        return false;
                    }
                    //check length
                    if (!hasUp && !hasLeft) {
                        if (hasDown) {
                            len = this.getVerticalLen(row, col);
                        }
                        else if (hasRight) {
                            len = this.getHorizonalLen(row, col);
                        }
                        else {
                            len = 1;
                        }
                        if (len > this.countMaxLength(ships)) {
                            return false;
                        }
                        if (final) {
                            if (ships[len] + 1 > this.maxShips[len]) {
                                return false;
                            }
                        }
                        else {
                            if (len < 5 && ships[len] + 1 > this.maxShips[len] + 1) {
                                return false;
                            }
                        }
                        ships[len]++;
                    }
                }
            }
            if (final) {
                for (l in ships) {
                    if (this.maxShips[l] != ships[l]) {
                        return false;        
                    }
                }
            }
            return true;
        },
        hasUp: function(row, col) {
            var nextR = row - 1;
            if (nextR in this.areas && this.areas[nextR][col] == battleConsts.states.ship) {
                return true;
            }
            return false;
        },
        hasDown: function(row, col) {
            var nextR = row + 1;
            if (nextR in this.areas && this.areas[nextR][col] == battleConsts.states.ship) {
                return true;
            }
            return false;
        },
        hasLeft: function(row, col) {
            var nextC = col - 1;
            if (nextC in this.areas[row] && this.areas[row][nextC] == battleConsts.states.ship) {
                return true;
            }
            return false;
        },
        hasRight: function(row, col) {
            var nextC = col + 1;
            if (nextC in this.areas[row] && this.areas[row][nextC] == battleConsts.states.ship) {
                return true;
            }
            return false;
        },
        hasVertical: function(row, col) {
            return this.hasUp(row, col) || this.hasDown(row, col);
        },
        hasHorizontal: function(row, col) {
            return this.hasRight(row, col) || this.hasLeft(row, col);
        },
        getVerticalLen: function(row, col) {
            var len = 1, i = row;
            while (this.hasUp(i, col)) {
                len++;
                i--;
            }
            i = row;
            while (this.hasDown(i, col)) {
                len++;
                i++;
            }
            return len;
        },
        getHorizonalLen: function(row, col) {
            var len = 1, i = col;
            while (this.hasLeft(row, i)) {
                len++;
                i--;
            }
            i = col;
            while (this.hasRight(row, i)) {
                len++;
                i++;
            }
            return len;
        },
        countMaxLength: function(ships) {
            var maxLength = 0;
            for (k in this.maxShips) {
                maxLength = maxLength < k && ships[k] < this.maxShips[k] ? k : maxLength;
            }
            return maxLength;
        },
        clear: function(row, col) {
            if (this.areas[row][col] == battleConsts.states.empty) {
                return;
            }
            this.areas[row][col] = battleConsts.states.empty;
            var hasUp = this.hasUp(row, col),
                hasDown = this.hasDown(row, col),
                hasLeft = this.hasLeft(row, col),
                hasRight = this.hasRight(row, col);
            if (hasUp) {
                this.clear(row - 1, col);
            }
            if (hasDown) {
                this.clear(row + 1, col);
            }
            if (hasLeft) {
                this.clear(row, col - 1);
            }
            if (hasRight) {
                this.clear(row, col + 1);
            }
        },
        exportShips: function() {
            ships = [];
            for (var row = 0; row < this.size; row++) {
                for (var col = 0; col < this.size; col++) {
                    if (
                        this.areas[row][col] == battleConsts.states.empty ||
                        this.hasUp(row, col) ||
                        this.hasLeft(row, col)
                    ) {
                        continue;
                    }
                    const lenv = this.getVerticalLen(row, col),
                          lenh = this.getHorizonalLen(row, col),
                          len = lenh > lenv ? lenh : lenv;
                    ships.push({
                        row: row, column:col, size: len, vertical: lenh < lenv ? true : false
                    });
                }
            }
            return ships;
        },
        isReady: function() {
            return this.areShipsInOrder(true);
        },
        shoot: function(row, col) {
            if (!this.started || !(row in this.areas) || !(col in this.areas[row][col])) {
                return false;
            }
            if (this.areas[row][col] == battleConsts.states.ship) {
                Vue.set(this.areas[row], col, battleConsts.states.blownship);
                return true;
            }
            else {
                if (this.areas[row][col] == battleConsts.states.empty) {
                    Vue.set(this.areas[row], col, battleConsts.states.blownempty);
                }
                return false;
            }
        }
    }
});
const battleFieldEnemy = battleFieldParent.extend({
    methods: {
        clicked: function(data) {
            if (!this.started) {
                return;
            }
            var row = data['row'], col = data['col'];
            if (this.areas[row][col] == battleConsts.states.empty) {
                this.$emit('shoot', {row: row, col: col});
            }
        }
    }
});
const battleSocket = {
    socket: null,
    opened: false,
    sent: false,
    connect: function(uuid) {
        this.socket = new WebSocket('ws://192.168.1.118:8888/game?uuid=' + uuid);
        this.socket.onopen = this.onOpen.bind(this);
        this.socket.onmessage = this.onMessage.bind(this);
        this.socket.onerror = this.onError.bind(this);
    },
    onOpen: function() {
        this.opened = true;
    },
    onError: function(error) {
        console.log('Error detected: ' + error);
    },
    onMessage: function (event) {
        this.sent = false;
        console.log(event.data);
    },
    send: function(data) {
        if (this.sent) {
            return;
        }
        this.sent = true;
        this.socket.send(JSON.stringify(data));
    },
    close: function() {
        this.socket.close();
    }
};
const vueApp = new Vue({
    el: '#vueApp',
    components: {
        'battlefieldown': battleFieldOwn,
        'battlefieldenemy': battleFieldEnemy
    },
    data: {
        size: 10,
        started: false,
        uuid: null,
        player: null
    },
    methods: {
        startAi: function() {
            if (this.started) {
                alert('Game already started!');
                return;
            }
            if (!this.$refs.ownfield.isReady()) {
                alert('Put ships!');
                return;
            }
            fetch('/start-ai', {method: 'POST', body: JSON.stringify(this.$refs.ownfield.exportShips())})
                .then(res => res.json())
                .catch(err => console.error(err))
                .then(this.onStarted);
                
        },
        startHuman: function() {
            alert('Doesn\'t work yet');return;
            fetch('/start', {method: 'POST', body: JSON.stringify(this.$refs.ownfield.exportShips())})
                .then(res => res.json())
                .catch(err => console.error(err))
                .then(this.onStarted);
        },
        onStarted: function(res) {
            if (res.success) {
                this.uuid = res.uuid;
                this.player = res.player;
                this.started = true;
                battleSocket.connect(this.uuid);
            }
            else {
                alert(res.message);
            }
        },
        shooted: function(data) {
            data['uuid'] = this.uuid;
            data['player'] = this.player;
            battleSocket.send(data);
        }
    }
});