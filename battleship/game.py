import uuid
import ai

FIELD_SIZE = 10
FIELD_SHIPS = 15
AREA_UNKNOWN = 0
AREA_EMPTY = 1
AREA_EMPTY_BLOWN = 2
AREA_HAS_SHIP = 3
AREA_SHIP_BLOWN = 4


class GameExeption(Exception):
    ''' '''
    pass


class Area():
    ''' '''

    row = None
    column = None
    value = None

    def __init__(self, row, column):
        '''
        @param row: int
        @param column: int
        '''
        self.row = row
        self.column = column
        self.value = AREA_EMPTY

    def isEmpty(self):
        '''
        @return: boolean
        '''
        return self.value == AREA_EMPTY

    def hasShip(self):
        '''
        @return: boolean
        '''
        return self.value == AREA_HAS_SHIP

    def placeShip(self):
        ''' '''
        if self.value == AREA_EMPTY:
            self.value = AREA_HAS_SHIP
        else:
            raise GameExeption('There is already ship here')

    def blow(self):
        '''
        @return: boolean
        '''
        if self.hasShip():
            self.value = AREA_SHIP_BLOWN
            return True
        else:
            if self.isEmpty():
                self.value = AREA_EMPTY_BLOWN
            return False

    def getValue(self, hide=False):
        '''
        @param hide: boolean
        @return: int
        '''
        if hide and (self.value == AREA_EMPTY or self.value == AREA_HAS_SHIP):
            return AREA_UNKNOWN
        return self.value


class Field():
    ''' '''

    areas = None
    ships = None
    #  size: count
    maxShips = {
        5: 1, 4: 2, 3: 3, 2: 4, 1: 5
    }

    def __init__(self):
        ''' '''
        self.areas = [
            [Area(r, c) for c in range(FIELD_SIZE)] for r in range(FIELD_SIZE)
        ]
        self.ships = {
            5: 0, 4: 0, 3: 0, 2: 0, 1: 0
        }

    def placeShips(self, data):
        '''
        @param data: list of dicts
        '''
        if len(data) > FIELD_SHIPS:
            raise GameExeption('Too many ships')
        for ship in data:
            self.placeShip(**ship)

    def shoot(self, row, column):
        '''
        @param row: int
        @param column: int
        @return: boolean
        '''
        self.checkCoords(row, column)
        return self.areas[row][column].blow()

    def export(self, own=True):
        '''
        @param own: boolean
        @return: list
        '''
        return [
            [a.getValue(not own) for a in row] for row in self.areas
        ]

    def isFinished(self):
        ''' '''
        for row in self.areas:
            for a in row:
                if a.hasShip():
                    return False
        return True

    def placeShip(self, row, column, size, vertical=True):
        '''
        @param row: int
        @param column: int
        @param size: int
        @param vertical: boolean
        '''
        if (size not in self.ships) or self.ships[size] >= self.maxShips[size]:
            raise GameExeption('Wrong ship size: {}'.format(size))

        self.checkCoords(row, column)

        if vertical:
            if row + size > FIELD_SIZE:
                raise GameExeption('Ship doesn\'t fit, row: {} size: {}'.format(row, size))
            modify = [self.areas[row + r][column] for r in range(size)]
        else:
            if column + size > FIELD_SIZE:
                raise GameExeption('Ship doesn\'t fit, column: {} size: {}'.format(column, size))
            modify = [self.areas[row][column + c] for c in range(size)]

        for a in modify:
            if not a.isEmpty():
                raise GameExeption('There is already ship here, row: {} column: {}'.format(row, column))
            if vertical:
                prv = a.column - 1
                nxt = a.column + 1
                if (prv > -1 and not self.areas[a.row][prv].isEmpty()) or \
                   (nxt < FIELD_SIZE and not self.areas[a.row][nxt].isEmpty()):
                    raise GameExeption('Wrong ship place, row: {} column: {}'.format(row, column))
            else:
                prv = a.row - 1
                nxt = a.row + 1
                if (prv > -1 and not self.areas[prv][a.column].isEmpty()) or \
                   (nxt < FIELD_SIZE and not self.areas[nxt][a.column].isEmpty()):
                    raise GameExeption('Wrong ship place, row: {} column: {}'.format(row, column))
        for a in modify:
            a.placeShip()
        self.ships[size] += 1

    def checkCoords(self, row, column):
        '''
        @param row: int
        @param column: int
        '''
        if row < 0 or row > FIELD_SIZE - 1:
            raise GameExeption('No such row')

        if column < 0 or column > FIELD_SIZE - 1:
            raise GameExeption('No such column')


class Game():
    ''' '''

    next = None
    fields = None

    def __init__(self, player1, player2):
        '''
        @param player1: str
        @param player2: str
        '''
        self.fields = {
            player1: Field(),
            player2: Field()
        }
        self.next = player1

    def placeShips(self, player, data):
        '''
        @param player: str
        @param data: list of dicts
        '''
        if player not in self.fields:
            raise GameExeption('Player not found')
        self.fields[player].placeShips(data)

    def shoot(self, player, row, column):
        '''
        @param player: str
        @param row: int
        @param column: int
        '''
        if player not in self.fields:
            raise GameExeption('Player not found')
        if player != self.next:
            raise GameExeption('It\'s not your turn')

        tmp = self.fields.keys()
        tmp.remove(self.next)
        other = tmp[0]

        res = self.fields[other].shoot(row, column)
        if res is False:
            self.next = other
        elif self.fields[other].isFinished():
            self.next = None

        return res

    def exportField(self, player, own=True):
        '''
        @param player: str
        @param own: boolean
        @return: list
        '''
        if player not in self.fields:
            raise GameExeption('Player not found')

        return self.fields[player].export(own)


class Match():
    ''' '''

    uuid = None
    game = None
    players = None

    def __init__(self):
        ''' '''
        self.uuid = uuid.uuid4()
        self.players = {
            str(uuid.uuid4()): None,
            str(uuid.uuid4()): None
        }
        players = tuple(self.players.keys())
        self.game = Game(players[0], players[1])

    def setPlayer(self, player):
        '''
        @param player: ?
        '''
        for uuid in self.players:
            if self.players[uuid] is None:
                self.players[uuid] = player
                return uuid
        raise GameExeption('Match already has two players')

    def placeShips(self, player, ships):
        ''' '''
        self.game.placeShips(player, ships)

    def onShoot(self, player, row, column):
        '''
        @param player: str
        @param row: int
        @param column: int
        '''
        res = self.game.shoot(player, row, column)
        for uuid in self.players:
            self.players[uuid].tell(row, column, res)
        return res


class MatchAi(Match):
    ''' '''

    aiKey = None

    def __init__(self):
        ''' '''
        super().__init__()
        self.aiKey = tuple(self.players.keys())[1]
        aip = ai.RandomAi()
        self.game.placeShips(self.aiKey, aip.putShips())
        self.players[self.aiKey] = aip

    def placeShips(self, ships):
        ''' '''
        self.game.placeShips(self.aiKey, ships)

    def onShoot(self, player, row, column):
        '''
        @param player: str
        @param row: int
        @param column: int
        '''
        res = super().onShoot(player, row, column)

        if not res:
            self._aiShoot()

    def getPlayerKey(self):
        ''' '''
        for uuid in self.players:
            return uuid

    def _aiShoot(self):
        ''' '''
        res = True
        while res:
            row, column = self.players[self.aiKey].shoot(self.game.fields[self.aiKey])
            res = super().onShoot(self.aiKey, row, column)
