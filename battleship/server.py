import os.path
import tornado.ioloop
import tornado.web
import tornado.escape
import tornado.websocket
import game

matches = {}


class IndexHandler(tornado.web.RequestHandler):
    ''' '''

    def get(self):
        ''' '''
        self.render('index.html')


class AjaxHandler():
    ''' '''

    def _send_error(self, msg):
        ''' '''
        self._send(tornado.escape.json_encode({
            'success': False,
            'message': msg
        }))

    def _send_success(self, data):
        ''' '''
        data['success'] = True
        self._send(tornado.escape.json_encode(data))


class StartAIHandler(tornado.web.RequestHandler, AjaxHandler):
    ''' '''

    def _send(self, msg):
        ''' '''
        self.write(msg)

    def post(self):
        ''' '''
        try:
            ships = tornado.escape.json_decode(self.request.body)
        except Exception:
            self._send_error('Wrong ships parameters')
            return

        match = game.MatchAi()
        try:
            match.placeShips(ships)
        except game.GameExeption as e:
            self._send_error('Wrong ships')
            return

        matches[match.uuid] = match
        self._send_success({
            'uuid': str(match.uuid),
            'player': match.getPlayerKey()
        })


class GameHandler(tornado.websocket.WebSocketHandler, AjaxHandler):
    ''' '''

    uuid = None

    def _send(self, msg):
        ''' '''
        self.write_message(msg)

    def open(self):
        ''' '''
        print('open')
        uuid = self.get_argument('uuid', None)
        if uuid not in matches:
            self.close(401, 'Invalid game identifier')
            return
        matches[uuid].setPlayer(self)

    def on_message(self, msg):
        ''' '''
        print('message')
        if not msg:
            self._send_error('Empty message')
            return
        try:
            msg = tornado.escape.json_decode(msg)
        except:
            self._send_error('Wrong JSON')
            return

        if 'uuid' not in msg or msg['uuid'] not in matches:
            self._send_error('Unknown game')
            self.close(401, 'Invalid game identifier')
            return

        match = matches[msg['uuid']]
        if 'player' in msg and 'row' in msg and 'column' in msg:
            try:
                match.onShoot(msg['player'], msg['row'], msg['column'])
            except game.GameExeption as e:
                self._send_error(str(e))
        else:
            self._send_error('Unknown message')

    def on_close(self):
        ''' '''
        print('close')
        # pass


def main():
    ''' '''

    settings = dict(
        cookie_secret='SX4gEWPE6bVr0vbwGtMl',
        template_path=os.path.join(os.path.dirname(__file__), 'templates'),
        static_path=os.path.join(os.path.dirname(__file__), 'static'),
        xsrf_cookies=False,
        debug=True
    )

    handlers = [
            (r'/', IndexHandler),
            (r'/start-ai', StartAIHandler),
            (r'/game', GameHandler),
            (r'/static/(.*)', tornado.web.StaticFileHandler, {'path': settings['static_path']})
    ]

    app = tornado.web.Application(handlers, **settings)
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()


if __name__ == '__main__':
    main()
