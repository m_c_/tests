from random import randint
import game


class Ai():
    pass


class RandomAi(Ai):
    ''' '''

    def putShips(self):
        '''
        @return: list of dicts
        '''
        field = game.Field()
        res = []
        for size in field.ships:
            for _ in range(1, field.ships[size]):
                res.append(self.putShip(field, size))
        return res

    def putShip(self, field, size):
        '''
        @param field: Field
        @param size: int
        @return: dict
        '''
        if size < 1 or size > 5:
            raise game.GameExeption('Wrong size of ship')
        mx = 100
        cnt = 0
        while(True):
            vertical = True if randint(0, 99) % 2 == 0 else False
            if vertical:
                row = randint(0, game.FIELD_SIZE - size - 1)
                col = randint(0, game.FIELD_SIZE - 1)
            else:
                row = randint(0, game.FIELD_SIZE - 1)
                col = randint(0, game.FIELD_SIZE - size - 1)
            try:
                field.placeShip(row, col, size, vertical)
                return {'row': row, 'column': col, 'size': size, 'vertical': vertical}
            except game.GameExeption as e:
                cnt += 1
                if cnt > mx:
                    raise e

    def shoot(self, field):
        '''
        @param field: list of lists
        @return: int,int
        '''
        size = pow(game.FIELD_SIZE - 1, 2)
        loop = 0
        while(True):
            row = randint(0, game.FIELD_SIZE - 1)
            col = randint(0, game.FIELD_SIZE - 1)
            if field[row][col] == game.AREA_UNKNOWN:
                return row, col
            else:
                loop += 1
                if loop > size:
                    return 0, 0
