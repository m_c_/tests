from bs4 import BeautifulSoup
import requests
import csv
import os
import time


PATH    = os.path.dirname(os.path.realpath(__file__))
TARGETS = PATH + '/targets.txt'
OUTPUT  = PATH + '/output.csv'
ROOT    = 'https://www.d7leadfinder.com'
SEARCH  = ROOT + '/getleads.php'
NEED    = ['Name', 'Phone', 'Emails', 'Address', 'Website']


def extract_data(soup, city, key):
    ''' '''

    with open(OUTPUT, 'a', newline='') as csvfile:
        writer = csv.writer(csvfile, dialect='excel')
        writer.writerow(['results for %s in %s' % (key, city)])
        writer.writerow(NEED)
        table = soup.find('table')
        if table is None:
            print('table not found')
            return
        table = soup.find('tbody')
        if table is None:
            print('tbody not found')
            return
        for tr in table.find_all('tr'):
            row = []
            for n in NEED:
                d = tr.find('td', attrs={'data-title': n})
                row.append(None if d is None else d.string)
            writer.writerow(row)
        writer.writerow([None]*len(NEED))


def search_one(city, key):
    ''' '''

    resp    = requests.get(ROOT)
    cookies = resp.cookies
    data    = {'city': city, 'keyword': key}
    headers = {
        'Referer': 'https://www.d7leadfinder.com/',
        'Host': 'www.d7leadfinder.com',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0'
    }
    resp = requests.post(SEARCH, data=data, headers=headers, cookies=cookies)
    return BeautifulSoup(resp.text, 'html.parser')


def main():
    ''' '''
    
    print('started...')
    with open(TARGETS) as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            print('get row...')
            if len(row) != 2:
                print('wrong input row: %s' % row)
                continue
            print('searching for %s in %s...' % (row[1], row[0]))
            found = search_one(row[0], row[1])
            print('extracting...')
            extract_data(found, row[0], row[1])
            time.sleep(5)


if __name__ == '__main__':
    main()
