"""
import psycopg2
from psycopg2.extras import RealDictCursor
from django.conf import settings
from django.http import HttpResponse
"""

import json
import datetime
import re
from uuid import uuid1

black_list_url = [
    'forbes.com',
    'usnews.com',
    'mcclatchydc.com',
    'thestreet.com',
    'bloomberg.com',
    'bloomberg.net', 
    'gamespot.com',
    'okayplayer.com',
    '/#!/',
    '/!#/',
    'http://hosted.ap.org',
    'twitter.com',
    'https://t.co',
    'opinion',
    'op-ed',
    '/oped/',
    'arabic-version',
]

# lowercase words
black_list_headline = [
    'opinion:',
    'opinion',
    'oped',
    'op-ed',
    'perspective',
    'watch',
    'video',
    'cnn video',
    'postlive',
    'analysis',
    'analysis:',
    'headlines for',
    'promo',
    '[promo]',
    '[sponsored]',
    'sponsored',
    'cnngo',
    'cnn poll',
    'cbs evening news',
    'mediator:',
    'mediator',
    'the tip sheet',
    "what's on tv",
    "what's on television",
    "what's on tonight",
    "whats on tv",
    "whats on television",
    "whats on tonight",
    "Fox News Brief",
    "Post Reports:",
    "Post Reports",
]

db_connection = None

def get_latest_news(request):
    fields = request.GET
    fields = {key:value for (key, value) in fields.items()}
    news = _get_news(fields)
    return HttpResponse(json.dumps(news, default=str), 'application/json')


def filter_black_list(item):
    for url_item in black_list_url:
        if url_item in item.get('url').lower():
            return False
    for hl_item in black_list_headline:
        if hl_item in item.get('title').lower():
            return False
    return True


def _whitespaces_to(string, oper):
    '''
    @param string: str
    @param oper: str
    @return: str
    '''
    start = 0
    i = 0
    while True:
        ind = string.find(' ', start)
        if ind == -1: 
            break
        start = ind + 1
        i += 1
        if string[ind:ind + 4] == ' or ' or \
           string[ind:ind + 5] == ' and ' or \
           string[ind - 3:ind + 1] == ' or ' or \
           string[ind - 4:ind + 1] == ' and ':
            pass
        else:
            string = string[0:ind] + oper + string[ind+1:]
        if start > len(string) or i > 1000:
            break
    return string


def _is_quote(string):
    ''' 
    @param string: str
    @return boolean
    '''
    last = len(string) - 1
    if string[0] == '"' and string[last] == '"':
        return True
    if string[0] == "'" and string[last] == "'":
        return True
    return False


def _no_quotes(string):
    '''
    @param string: str
    @return: str
    '''
    return string.replace("'", '').replace('"', '')


def _parse_field(string):
    '''
    @param string: str
    @return str
    '''
    string = _no_quotes(string).strip().lower()

    # whitespace is OR
    string = _whitespaces_to(string, ' or ')

    # checking conditions
    ored = []
    for part in map(str.strip, string.split(' or ')):
        ored.append(map(str.strip, part.split(' and ')))

    # construct condition 
    parts = []
    for anded in ored:
        tmp = []
        for part in anded:
            if part == 'or':
                continue
            field = 'domain_id' if part.find('.') > -1 else 'source_name'
            tmp.append("{} ILIKE '%{}%'".format(field, part))
        parts.append('(' + ' and '.join(tmp) + ')')
    return ' or '.join(parts)


def _parse_keyword(string):
    ''' 
    @param string: str
    @return string
    '''
    string = string.strip()
    
    # if whole search is quote
    if _is_quote(string):
        string = _no_quotes(string)
        return "title ILIKE '%{}%' or description ILIKE '%{}%'".format(string, string)

    # quotes
    quotes = re.findall(r'".+?"', string)
    quotes_replace = {}
    if quotes:
        for quote in quotes:
            uid = uuid1()
            quotes_replace[str(uid)] = _no_quotes(quote)
            string = string.replace(quote, str(uid))

    # whitespace is AND
    string = _whitespaces_to(string, ' and ')

    # checking conditions
    string = string.lower()
    ored = []
    for part in map(str.strip, string.split(' or ')):
        tmp = []
        for t in map(str.strip, part.split(' and ')):
            if t == 'or':
                continue
            if t in quotes_replace:
                t = quotes_replace[t]
            tmp.append(_no_quotes(t))
        ored.append(tmp)

    # construct condition 
    parts = []
    for anded in ored:
        tmp = []
        for part in anded:
            tmp.append("(title ILIKE '%{}%' or description ILIKE '%{}%')".format(part, part))
        if len(tmp) == 1:
            parts.append(tmp[0])
        else:
            parts.append('(' + ' and '.join(tmp) + ')')
    return ' or '.join(parts)


def _get_news(fields):
    '''
    @param fields: dict
    @return: list
    '''
    """
    global db_connection
    db_connection = psycopg2.connect(
        database=settings.NEWSAPI_DB_NAME, user=settings.NEWSAPI_DB_USER,
        password=settings.NEWSAPI_DB_PASS, host=settings.NEWSAPI_DB_HOST
    )
    cur = db_connection.cursor(cursor_factory=RealDictCursor)
    """
    statement = "SELECT * FROM news WHERE ({condition}) AND publishedat > current_date-1 AND title!='' ORDER BY publishedat DESC LIMIT 40;"

    if 'keyword' in fields:
        condition = _parse_keyword(fields['keyword'])

    elif 'source_name' in fields:
        condition = _parse_field(fields['source_name'])

    elif 'domain_id' in fields:
        condition = _parse_field(fields['domain_id'])

    elif 'source_id' in fields:
        condition = "source_id ILIKE '%{}%'".format(_no_quotes(fields['source_id']))

    else:
        print('Wrong parameters')
        return []

    print(condition)
    """
    statement = statement.format(condition=condition)
    cur.execute(statement, fields)
    news = cur.fetchall()
    cur.close()
    news = list(filter(filter_black_list, news))
    return news
    """
tests = (
    {'keyword': 'trump'},
    {'keyword': 'trump "apple cool'},
    {'keyword': 'trump apple cool'},
    {'keyword': 'trump or cool'},
    {'keyword': 'trump and cool'},
    {'keyword': 'trump "apple pie" cool'},
    {'source_name': 'cnn'},
    {'domain_id': 'cnn.com'},
    {'domain_dat_id': 'cnn.com'},
    {'source_name': 'cnn and bbc and somth'},
    {'source_name': 'cnn and bbc or somth'},
    {'source_name': 'cnn and bbc.com'},
    {'source_name': 'cnn bbc.com'},
    {'source_name': 'cnn bbc'},
    {'source_name': 'cnn or bbc'},
    {'domain_id': 'cnn.com and bbc.com'},
    {'domain_id': 'cnn.com or bbc.com'},
    {'domain_id': 'cnn.com and bbc'},
    {'source_id': 'http://cnn.com/page'},
)
for test in tests:
    print(test)
    _get_news(test)

#statement = statement.format(condition=condition)
#cur.execute(statement)
