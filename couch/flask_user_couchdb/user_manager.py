import datetime

from flask import abort, Blueprint, current_app, Flask, session
from flask_login import LoginManager
from wtforms import ValidationError

from flask_user import ConfigError
from flask_user import forms
from flask_user.db_manager import DBManager
from flask_user.email_manager import EmailManager
from flask_user.password_manager import PasswordManager
from flask_user.token_manager import TokenManager
from flask_user.translation_utils import lazy_gettext as _ # map _() to lazy_gettext()
from flask_user import UserManager
from flask_user.user_manager__settings import UserManager__Settings
from flask_user.user_manager__utils import UserManager__Utils
from flask_user.user_manager__views import UserManager__Views

from .db_manager import CouchDbDBManager


class CouchDbUserManager(UserManager):
    '''
    User manager with support of DbManager for CouchDb
    '''

    def init_app(
        self, app, db, UserClass,
        UserInvitationClass=None,
        UserEmailClass=None,
        RoleClass=None, # Only used for testing
        ):

        # See http://flask.pocoo.org/docs/0.12/extensiondev/#the-extension-code
        # Perform Class type checking
        if not isinstance(app, Flask):
            raise TypeError("flask_user.UserManager.init_app(): Parameter 'app' is an instance of class '%s' "
                            "instead of a subclass of class 'flask.Flask'."
                            % app.__class__.__name__)

        # Bind Flask-User to app
        app.user_manager = self

        # Remember all data-models
        # ------------------------
        self.db = db
        # self.db_manager.UserClass = UserClass
        # self.db_manager.UserEmailClass = UserEmailClass
        # self.UserInvitationClass = UserInvitationClass
        # self.RoleClass=RoleClass

        # Load app config settings
        # ------------------------
        # For each 'UserManager.USER_...' property: load settings from the app config.
        for attrib_name in dir(self):
            if attrib_name[0:5] == 'USER_':
                default_value = getattr(UserManager, attrib_name)
                setattr(self, attrib_name, app.config.get(attrib_name, default_value))

        # If USER_EMAIL_SENDER_EMAIL is not set, try to construct it from
        # MAIL_DEFAULT_SENDER or DEFAULT_MAIL_SENDER
        if not self.USER_EMAIL_SENDER_EMAIL:
            default_sender = app.config.get('DEFAULT_MAIL_SENDER', None)
            default_sender = app.config.get('MAIL_DEFAULT_SENDER', default_sender)
            if default_sender:
                # Accept two formats: '{name}<{email}>' or plain '{email}'
                if default_sender[-1:] == '>':
                    start = default_sender.rfind('<')
                    if start >= 1:
                        self.USER_EMAIL_SENDER_EMAIL = default_sender[start + 1:-1]
                        if not self.USER_EMAIL_SENDER_NAME:
                            self.USER_EMAIL_SENDER_NAME = default_sender[0:start].strip(' "')
                else:
                    self.USER_EMAIL_SENDER_EMAIL = default_sender

        # If USER_EMAIL_SENDER_NAME is not set, default it to USER_APP_NAME
        if not self.USER_EMAIL_SENDER_NAME:
            self.USER_EMAIL_SENDER_NAME = self.USER_APP_NAME

        # Configure Flask session behavior
        # --------------------------------
        if self.USER_USER_SESSION_EXPIRATION:
            app.permanent_session_lifetime = datetime.timedelta(seconds=self.USER_USER_SESSION_EXPIRATION)

            @app.before_request
            def advance_session_timeout():
                session.permanent = True # Timeout after app.permanent_session_lifetime period
                session.modified = True # Advance session timeout each time a user visits a page

        # Configure Flask-Login
        # --------------------
        # Setup default LoginManager using Flask-Login
        self.login_manager = LoginManager(app)
        self.login_manager.login_view = 'user.login'

        # Flask-Login calls this function to retrieve a User record by token.
        @self.login_manager.user_loader
        def load_user_by_user_token(user_token):
            user = self.db_manager.UserClass.get_user_by_token(user_token)
            return user

        # Configure Flask-BabelEx
        # -----------------------
        self.babel = app.extensions.get('babel', None)
        from flask_user.translation_utils import init_translations
        init_translations(self.babel)

        # Configure Jinja2
        # ----------------
        # If the application has not initialized BabelEx,
        # we must provide a NULL translation to Jinja2
        if not hasattr(app.jinja_env, 'install_gettext_callables'):
            app.jinja_env.add_extension('jinja2.ext.i18n')
            app.jinja_env.install_null_translations()

        # Define a context processor to provide custom variable and functions available to Jinja2 templates
        def flask_user_context_processor():

            # In Flask-Login 0.2 ``is_authenticated`` and ``is_active`` were implemented as functions,
            # while in 0.3+ they are implemented as properties.
            def call_or_get(function_or_property):
                return function_or_property() if callable(function_or_property) else function_or_property

            return dict(
                user_manager=current_app.user_manager,
                call_or_get=call_or_get,
            )

        # Register context processor with Jinja2
        app.context_processor(flask_user_context_processor)

        # Create a dummy Blueprint to add the app/templates/flask_user dir to the template search path
        blueprint = Blueprint('flask_user', __name__, template_folder='templates')
        app.register_blueprint(blueprint)

        # Set default form classes
        # ------------------------
        self.AddEmailFormClass = forms.AddEmailForm
        self.ChangePasswordFormClass = forms.ChangePasswordForm
        self.ChangeUsernameFormClass = forms.ChangeUsernameForm
        self.EditUserProfileFormClass = forms.EditUserProfileForm
        self.ForgotPasswordFormClass = forms.ForgotPasswordForm
        self.InviteUserFormClass = forms.InviteUserForm
        self.LoginFormClass = forms.LoginForm
        self.RegisterFormClass = forms.RegisterForm
        self.ResendEmailConfirmationFormClass = forms.ResendEmailConfirmationForm
        self.ResetPasswordFormClass = forms.ResetPasswordForm

        # Set default managers
        # --------------------
        # Setup DBManager
        self.db_manager = CouchDbDBManager(app, db, UserClass, UserEmailClass, UserInvitationClass, RoleClass)

        # Setup PasswordManager
        self.password_manager = PasswordManager(app)

        # Set default EmailAdapter
        if self.USER_ENABLE_EMAIL:
            from flask_user.email_adapters.smtp_email_adapter import SMTPEmailAdapter
            self.email_adapter = SMTPEmailAdapter(app)

        # Setup EmailManager
        if self.USER_ENABLE_EMAIL:
            self.email_manager = EmailManager(app)

        # Setup TokenManager
        self.token_manager = TokenManager(app)

        # Allow developers to customize UserManager
        self.customize(app)

        # Make sure the settings are valid -- raise ConfigError if not
        self._check_settings(app)

        # Configure a list of URLs to route to their corresponding view method.
        self._add_url_routes(app)
