from flask_user import UserMixin
from flaskext.couchdb import Document, BooleanField, TextField, ListField


# Define the User document.
# NB: Make sure to add flask_user UserMixin !!!
class User(Document, UserMixin):
    ''' '''
    doc_type = 'user'

    active = BooleanField(default=True)

    # User authentication information
    username = TextField()
    password = TextField()

    # Relationships
    roles = ListField(TextField())
