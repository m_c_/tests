from flask import g
from flask_user.db_adapters import DbAdapterInterface


class CouchDbAdapter(DbAdapterInterface):
    '''
    Implements the DbAdapter interface to find, add, update and delete
    database objects using Flask-CouchDB
    '''

    def __init__(self, app, db):
        '''
        @param app: The Flask appliation instance
        @param db: The CouchDBManager object-database mapper instance
        '''
        # This no-op method is defined to show it in Sphinx docs in order 'bysource'
        super(CouchDbAdapter, self).__init__(app, db)

    def add_object(self, obj):
        '''
        @param obj: flask_user_couchdb.user.User instance
        '''
        obj.store()

    def get_object(self, obj_class, id):
        '''
        @param obj_class: flask_user_couchdb.user.User
        @param obj_id:
        @return: flask_user_couchdb.user.User instance or None
        '''
        doc = obj_class.load(id)
        if doc:
            return doc
        return None

    def find_objects(self, obj_class, **kwargs):
        '''
        case sensitive
        @param obj_class: flask_user_couchdb.user.User
        @param kwargs:
        @return: list
        '''
        objs = []
        for doc in g.couch.find({'selector': kwargs}):
            objs.append(obj_class.wrap(doc))
        return objs

    def find_first_object(self, obj_class, **kwargs):
        '''
        case sensitive
        @param obj_class: flask_user_couchdb.user.User
        @param kwargs:
        @return: flask_user_couchdb.user.User instance or None
        '''
        objs = self.find_objects(obj_class, **kwargs)
        return objs[0] if objs else None

    def ifind_first_object(self, obj_class, **kwargs):
        '''
        If USER_IFIND_MODE is 'nocase_collation' then case sensitive
        If USER_IFIND_MODE is 'ifind' then case insensitive
        @param obj_class: flask_user_couchdb.user.User
        @param kwargs:
        @return: flask_user_couchdb.user.User instance or None
        '''
        if self.user_manager.USER_IFIND_MODE == 'nocase_collation':
            return self.find_first_object(obj_class, **kwargs)

        iexact_kwargs = {}
        for key, value in kwargs.items():
            iexact_kwargs[key] = {'$regex': '(?i)^{}$'.format(value)}

        return self.find_first_object(obj_class, **iexact_kwargs)

    def save_object(self, obj, **kwargs):
        '''
        @param obj: flask_user_couchdb.user.User instance
        '''
        obj.store()

    def delete_object(self, obj):
        '''
        @param obj: flask_user_couchdb.user.User instance
        '''
        g.couch.delete(obj)

    def commit(self):
        ''' '''
        g.couch.commit()

    # Database management methods
    # ---------------------------

    def create_all_tables(self):
        '''This method does nothing for CouchDbAdapter'''
        pass

    def drop_all_tables(self):
        '''
        Drop all document collections of the database
        warning:: ALL DATA WILL BE LOST. Use only for automated testing
        '''
        for doc in g.couch.find({'selector': {'doc_type': 'user'}}):
            self.delete_object(doc)
