from flask_user.db_manager import DBManager
from flask_user.db_adapters import PynamoDbAdapter, DynamoDbAdapter, MongoDbAdapter, SQLDbAdapter
from flask_user import current_user, ConfigError
from .db_adapter import CouchDbAdapter


class CouchDbDBManager(DBManager):
    '''
    Db manager with support for CouchDb
    '''

    def __init__(self, app, db, UserClass, UserEmailClass=None, UserInvitationClass=None, RoleClass=None):
        ''' '''
        """Initialize the appropriate DbAdapter, based on the ``db`` parameter type.

        Args:
            app(Flask): The Flask application instance.
            db: The Object-Database Mapper instance.
            UserClass: The User class.
            UserEmailClass: Optional UserEmail class for multiple-emails-per-user feature.
            UserInvitationClass: Optional UserInvitation class for user-invitation feature.
            RoleClass: For testing purposes only.
        """
        self.app = app
        self.db = db
        self.UserClass = UserClass
        self.UserEmailClass = UserEmailClass
        self.UserInvitationClass = UserInvitationClass
        self.RoleClass = RoleClass

        self.user_manager = app.user_manager
        self.db_adapter = None

        # Check if db is a SQLAlchemy instance
        if self.db_adapter is None:
            try:
                from flask_sqlalchemy import SQLAlchemy

                if isinstance(db, SQLAlchemy):
                    self.db_adapter = SQLDbAdapter(app, db)
            except ImportError:
                pass # Ignore ImportErrors

        # Check if db is a MongoEngine instance
        if self.db_adapter is None:
            try:
                from flask_mongoengine import MongoEngine

                if isinstance(db, MongoEngine):
                    self.db_adapter = MongoDbAdapter(app, db)
            except ImportError:
                pass # Ignore ImportErrors

        # Check if db is a Flywheel instance
        if self.db_adapter is None: # pragma: no cover
            try:
                from flask_flywheel import Flywheel

                if isinstance(db, Flywheel):
                    self.db_adapter = DynamoDbAdapter(app, db)
            except ImportError:
                pass # Ignore ImportErrors

        # Check if the UserClass is a Pynamo Model
        if self.db_adapter is None:
            try:
                from pynamodb.models import Model

                if issubclass(UserClass, Model):
                    self.db_adapter = PynamoDbAdapter(app)
            except ImportError:
                pass # Ignore ImportErrors

        # Check if the UserClass is a Pynamo Model
        if self.db_adapter is None:
            try:
                from flaskext.couchdb import Document

                if issubclass(UserClass, Document):
                    self.db_adapter = CouchDbAdapter(app, db)
            except ImportError:
                pass # Ignore ImportErrors

        # Check self.db_adapter
        if self.db_adapter is None:
            raise ConfigError(
                'No Flask-CouchDb, Flask-SQLAlchemy, Flask-MongoEngine or Flask-Flywheel installed and no Pynamo Model in use.'\
                ' You must install one of these Flask extensions.')
