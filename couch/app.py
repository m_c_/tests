from flask import Flask, render_template_string
from flaskext.couchdb import CouchDBManager
from flask_user import login_required
from flask_user_couchdb.user import User
from flask_user_couchdb import CouchDbUserManager


# Class-based application configuration
class ConfigClass(object):
    ''' Flask application config '''

    # Flask settings
    SECRET_KEY = 'This is an INSECURE secret!! DO NOT use this in production!!'

    # Flask-CouchDb settings
    COUCHDB_DATABASE = 'tst_app'
    COUCHDB_SERVER = 'http://admin:coucher@localhost:5984'

    # Flask-User settings
    USER_APP_NAME = 'Flask-User CouchDB App' # Shown in and email templates and page footers
    USER_ENABLE_EMAIL = False # Disable email authentication
    USER_ENABLE_USERNAME = True # Enable username authentication
    USER_REQUIRE_RETYPE_PASSWORD = False # Simplify register form
    USER_ALLOW_LOGIN_WITHOUT_CONFIRMED_EMAIL = True


def create_app():
    ''' Flask application factory '''

    # Setup Flask and load app.config
    app = Flask(__name__)
    app.config.from_object(__name__ + '.ConfigClass')

    # Setup Flask-CouchDB
    db = CouchDBManager()
    db.setup(app)

    # Setup Flask-User and specify the User data-model
    user_manager = CouchDbUserManager(app, db, User)

    # The Home page is accessible to anyone
    @app.route('/')
    def home_page():
        # String-based templates
        return render_template_string('''
            {% extends 'flask_user_layout.html' %}
            {% block content %}
                <h2>Home page</h2>
                <p><a href={{ url_for('user.register') }}>Register</a></p>
                <p><a href={{ url_for('user.login') }}>Sign in</a></p>
                <p><a href={{ url_for('home_page') }}>Home page</a> (accessible to anyone)</p>
                <p><a href={{ url_for('member_page') }}>Member page</a> (login required)</p>
                <p><a href={{ url_for('user.logout') }}>Sign out</a></p>
            {% endblock %}
            ''')

    # The Members page is only accessible to authenticated users via the @login_required decorator
    @app.route('/members')
    @login_required # User must be authenticated
    def member_page():
        # String-based templates
        return render_template_string('''
            {% extends 'flask_user_layout.html' %}
            {% block content %}
                <h2>Members page</h2>
                <p><a href={{ url_for('user.register') }}>Register</a></p>
                <p><a href={{ url_for('user.login') }}>Sign in</a></p>
                <p><a href={{ url_for('home_page') }}>Home page</a> (accessible to anyone)</p>
                <p><a href={{ url_for('member_page') }}>Member page</a> (login required)</p>
                <p><a href={{ url_for('user.logout') }}>Sign out</a></p>
            {% endblock %}
            ''')

    return app


# Start development web server
if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0', port=5000, debug=True)
